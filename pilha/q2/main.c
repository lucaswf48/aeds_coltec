
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "lwfunc.h"

int avaliaExpressao(char *exp);

int main(){

	char *string;

	printf("Digite a expressão matemática:");
	string = lerStringN();
	
	if(avaliaExpressao(string))
		printf("Expressão valida\n");
	else
		printf("Expressão invalida\n");
	
	


	free(string);	
	return 0;
}


int avaliaExpressao(char *exp){

	int cont;
	Pilha *paren = pilhaCria();
	for(cont = 0;cont < strlen(exp);++cont){
		
		if(exp[cont] == ')'){
				
			if(pilhaVazia(paren)){
				return 0;
			}
			else{
				pilhaPop(paren);
			}
		}
		else if(exp[cont] == '('){
			pilhaPush(paren,exp[cont]);
		}

	}
	
	if(pilhaVazia(paren))
		return 1;
	else
		return 0;
}