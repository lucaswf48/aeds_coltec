#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "func.h"


//Se o sistema operacional for linux
#ifdef linux

//Cria a função para limpar o buffer com o __fpurge(stdin)
void limpaBuffer(){

	__fpurge(stdin);
}

//Cria a função que limpa a tela com system("clear")
void limpaTela(){

	system("clear");
}

//Se o sistema operacional for windows
#elif WIN32


//Cria a função para limpar o buffer com o fflush(stdin)
void limpaBuffer(){

	fflush(stdin);
}

//Cria a função que limpa a tela com system("cls")
void limpaTela(){

	system("cls");
}

#endif

//Função que segura o resultado na tela
void continua(){

    printf("\nAperte \"ENTER\" para continuar");
    limpaBuffer();
    getchar();
    limpaTela();
}

/*==========  Funções e procedimentos do tipo grafo  ==========*/


//Função que retorna uma struct do tipo Grafo alocada dinamicamente
Grafo* alocaGrafo(){

	return (Grafo*) malloc(sizeof(Grafo));
}


//Procedimento que aloca a matriz de adjacencia
//Recebe como argumento o grafo, um inteiro com o numero de vertices, um bool
//que indica se o grafo é orientado ou não e outro bool que indica se é ponderado ou não
void alocaMatrizAdjacente(Grafo *g, int n,bool ori,bool pon){

	int cont;


//Aqui a matriz é alocada
	g->matriz = (int**)calloc(n,sizeof(int*));
	for(cont = 0;cont < n;++cont)
		g->matriz[cont] = (int*)calloc(n,sizeof(int));

	g->numero_vertices = n;
	g->orientado = ori;
	g->ponderado = pon;
}


//Procedimento que imprime a matriz de adjacencia
void imprimeMatrizAdjacente(Grafo *g){

	int j,i;

	for(i = 0;i < g->numero_vertices;++i){
		//Imprime o vertice
		printf("%d: ",i+1);
		for(j = 0;j < g->numero_vertices;++j){

			printf("%2d ",g->matriz[i][j]);
		}
		printf("\n");
	}
}

//Função que insere uma aresta no grafo e retornA o mesmo
Grafo* insereMarizAdjacente2(Grafo *g, int vertice1, int vertice2,int peso){

			//É subtraido um para a posição na matriz ficar correta.(Vertice 1 fica na posição 0 na matriz)
			g->matriz[vertice1-1][vertice2-1] = peso;
			g->matriz[vertice2-1][vertice1-1] = peso;
			return g;
}

//Função que avalia se as arestas a serem inseridas estão corretas(dentro do limite da matriz) e retorn um bool
bool avaliaVertices(Grafo *g,int a,int b){

	//Se os vertices estiverem dentro do limite retorna verdadeiro
	if((a >=0 && a <= g->numero_vertices) && (b >=0 && b <= g->numero_vertices))
		return true;
	//Senão retorna falso
	else
		return false;
}

//Função que chama o algoritmo de kruskal se a rede não estiver vazia e retorna NULL
Grafo* solucao(Grafo *g){

	if(g==NULL){
		printf("Rede vazia!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		return NULL;
	}
	else
		return Kruskal(g);
}

//Função simples
//Então não vou comentar mais
Grafo* remover(Grafo *g){

	int escolha;

	limpaTela();

	do{

		menu3();

		scanf("%d",&escolha);

		switch(escolha){
				case 1:
					free(g);
					g = NULL;
					printf("Rede liberada");
					return g;
					break;
				case 2:
					g = retiraAresta(g);
					break;
				case 0:
					return g;
					break;
				default:
					printf("OPÇÃO INVALIDA!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
					break;
		}

		continua();

	}while(escolha != 0);

	return g;
}

//Função que retira um fio da rede, caso o usuario queira
Grafo* retiraAresta(Grafo *g){

	//Verifica se a rede esta vazia
	if(g==NULL){
		printf("REDE VAZIA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	}
	else{

		int rot1, //Roteador de "origem"
			  rot2; //Roteador de "destino"

		printf("A quais roteadores o cabo a ser retirado esta ligado?(Dois inteiros separados por um espaço)\n");
		scanf("%d %d",&rot1,&rot2);

		//Verifica se os vertices estão dentro do limite da matriz
		if(avaliaVertices(g,rot1,rot2)){
			g->matriz[rot1-1][rot2-1] = 0;
			g->matriz[rot2-1][rot1-1] = 0;
		}
		else{
			printf("Roteadoes invalidos!!!!!!!!!!!!!!!!\n");
		}
	}

	//Retorna o grafo com as modificações
	return g;
}


//Função que lê de um arquivo a rede
Grafo* lerArquivoURI(Grafo *g){


	FILE *f;							 //Arquivo rede.txt

	int rot1,              //Roteador de "origem"
			rot2,							 //Roteador de "destino"
			preco,             //Preço de cada cabo
			roter;             //Numero de roteadores

	//Abre o arquivo rede.txt
	f = fopen("rede.txt","r");

	//Libera a rede anterior
	free(g);

	//Aloca uma nova rede
	g = alocaGrafo();

	//Lê do arquivo o número de roteadores
	fscanf(f,"%d",&roter);

	//Aloca a matriz adjacente
	alocaMatrizAdjacente(g,roter,false,true); //Não dircionada, e ponderada

	do{

		//Lê do arquivo os roteadores e o preço de cada cabo
		fscanf(f,"\n%d %d %d",&rot1,&rot2,&preco);

		//Avalia se os roteadores estão dentro do limite da matriz
		if(avaliaVertices(g,rot1,rot2)){
			//Se estiver insere na matriz
			g = insereMarizAdjacente2(g,rot1,rot2,preco);
		}
		else{
			//Senão estiver nos limites imprimi esta mensagem, libera a rede ja criada e retorna NULL
			printf("Algum roteador é invalido        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			printf("Modifique o arquivo rede.txt          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!t\n");
			free(g);
			return NULL;
		}

	}while(!feof(f));

	//Fecha o arquivo rede.txt
	fclose(f);

	//Retorna rede criada
	return g;
}

//Função que imprime a matriz de adjacencia se ela exitir
void imprimir(Grafo *g){

	if(g==NULL)
		printf("REDE VAZIA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	else
		imprimeMatrizAdjacente(g);
}



Grafo* Kruskal(Grafo *g){

	int min,i,j,
		custo_minimo = 0,
		a=0,
		b=0,
		u=0,
		v=0,
		ne=1;

	int *parent = (int*)calloc(g->numero_vertices,sizeof(int));


	for(i=0;i<g->numero_vertices;i++)
	{
		for(j=0;j<g->numero_vertices;j++)
		{
			if(g->matriz[i][j]==0)
				g->matriz[i][j]=1000;
		}
	}

	while(ne < g->numero_vertices)
	{

		for(i=0,min=999;i<g->numero_vertices;i++)
		{
			for(j=0;j < g->numero_vertices;j++)
			{

				if(g->matriz[i][j] < min)
				{

					min=g->matriz[i][j];
					a=u=i;
					b=v=j;

				}
			}
		}
		u=find(u,parent);
		v=find(v,parent);
		if(uni(v,u,parent))
		{
			printf("Fio que liga %d a %d: R$ %d\n",a+1,b+1,min);
			custo_minimo +=min;
			ne++;
		}
		g->matriz[a][b]=g->matriz[b][a]=999;
	}
	free(g);
	g = NULL;
	printf("\nCusto minimo = %d\n",custo_minimo);

	return g;
}


int find(int i,int v[]){

	while(v[i])
	i=v[i];
	return i;
}
int uni(int i,int j,int v[]){


	if(i!=j)
	{
		v[j]=i;
		return 1;
	}
	return 0;
}
