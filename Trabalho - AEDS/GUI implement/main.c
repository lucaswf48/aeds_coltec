#include <Python.h> //Precisa do pacote python-dev
#include <stdbool.h>
#include "func.h"

//COMENTE O CODIGO


void menu1();
void menu2();
void menu3();


int main(int argc, char *argv[]){

    int escolha;

    Grafo *rede = NULL;

    do{
        limpaTela();

        menu1();

        scanf("%d",&escolha);
        switch(escolha){
        	case 2:
        		imprimir(rede);
        		break;
        	case 3:
        		rede = remover(rede);
        		break;
        	case 4:
        		rede = solucao(rede);
        		break;
        	case 1:
        		rede = lerArquivoURI(rede);
        		break;
        }
        continua();
    }while(escolha != 0);

    free(rede);

    return 0;
}

void menu1(){

	printf("Escolha uma opção:\n");
	printf("1 - Ler a rede de um arquivo\n");
	printf("2 - Imprimir\n");
	printf("3 - Remover\n");
	printf("4 - Solução\n");
	printf("0 - Sair do programa\n");
}

void menu3(){

	printf("Escolha uma opção:\n");
	printf("1 - Liberar rede\n");
	printf("2 - Para retirar aresta especifica\n");
	printf("0 - Sair do programa\n");
}
