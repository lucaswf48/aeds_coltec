/*==========  Struct grafo  ==========*/

typedef struct grafo{

	bool orientado; 				//Determina se o grafo é orientado
	bool ponderado;					//Determina se o grafo é ponderado
	int numero_vertices;    //Determina o número de vertices
	int **matriz;           //Matriz de adjacencia

}Grafo;


/*==========  Limpar tela e limpar buffer  ==========*/



void limpaTela();
void limpaBuffer();
void continua();


/*==========  Funções e procedimentos do tipo grafo  ==========*/

Grafo* alocaGrafo();
void alocaMatrizAdjacente(Grafo *g, int n,bool ori,bool pon);
void imprimeMatrizAdjacente(Grafo *g);
Grafo* insereMarizAdjacente2(Grafo *g, int vertice1, int vertice2,int peso);
bool avaliaVertices(Grafo *g,int a,int b);


/*==========  Funções e procedimentos do trabalho  ==========*/

void imprimir(Grafo *g);
Grafo* remover();
Grafo* solucao(Grafo *g);
Grafo* lerArquivoURI();
Grafo* retiraAresta(Grafo *g);
int find(int i,int v[]);
int uni(int i,int j,int v[]);
Grafo* Kruskal(Grafo *g);




/* https://www.youtube.com/playlist?list=PLBBD45666C52032EF */
