# -*- coding: utf-8 -*-
from Tkinter import *
import tkMessageBox
import Tkinter
import sys

def num_callback():
    rede_num=num_entry.get()
    rede_num=int(rede_num, base=10)    

root = Tk()
frame = Frame(root)
frame.pack()

rightframe = Frame(root)
rightframe.pack( side = RIGHT )
leftframe = Frame(root)
leftframe.pack( side = LEFT )


L1 = Label(leftframe, text="Número de Roteadores")
L1.pack( side = LEFT)
num_entry = Spinbox(root, from_=1, to=20, width=3)
B = Tkinter.Button(rightframe, text ="Confirmar", command = num_callback)
B.pack(side = RIGHT)
num_entry.pack(side = RIGHT)

root.mainloop()
