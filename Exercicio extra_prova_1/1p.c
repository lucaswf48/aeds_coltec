#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"

void pega(Lista_int *jogador_atual,int a);

int main(){

	int cartas,cont=0,q=0,
		jogadores,
		conta_cartas,conta_jogadores,
		*quais_cartas;

	scanf("%d %d",&cartas,&jogadores);
	if(cartas == 0 && jogadores == 0)
			exit(0);
	if(cartas < 2 || cartas > 10000 || jogadores < 2 || jogadores > 20 || jogadores > cartas){
			
		printf("Entrada invalida. Modifique o arquivo\n");
		exit(0);
	}
	Pilha *pilha_das_monte = pilha_cria();
	Lista_int *lista_de_jogadores = lst_cria(),*lista_aux,*area = lst_cria();


	quais_cartas = (int*)calloc(cartas,sizeof(int));
	for(conta_jogadores=jogadores;conta_jogadores > 0;conta_jogadores--){
			lista_de_jogadores = lst_insere(lista_de_jogadores,conta_jogadores);
			lista_de_jogadores->cartas = pilha_cria();
	}

		for(lista_aux = lista_de_jogadores;lista_aux != NULL;lista_aux=lista_aux->prox){
		
			if(lista_aux->prox==NULL){
				lista_aux->prox=lista_de_jogadores;
				break;
			}
		}

		

	for(conta_cartas=0;conta_cartas<cartas;conta_cartas++){
			scanf("%d",&quais_cartas[conta_cartas]);
	}
	for(conta_cartas=cartas - 1;conta_cartas>=0;conta_cartas--){
			pilha_push(pilha_das_monte,quais_cartas[conta_cartas]);
	}
	



	Lista_int *p = lista_de_jogadores;
	int carta;
	if(p)
		do{
			
			carta = pilha_pop(pilha_das_monte);
			pega(p,carta);
			p=p->prox;
		}while(!pilha_vazia(pilha_das_monte));

	
	do{
			
			printf("Jogador: %d\n",p->info );
			printf("Descarte\n");			
			printf("%.0f\n",pilha_pop(p->cartas));			
			p=p->prox;
	}while(!pilha_vazia(p->cartas));	

}

void pega(Lista_int *jogador_atual,int a){

	pilha_push(jogador_atual->cartas,a);
	
}