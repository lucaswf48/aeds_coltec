
#include <stdio.h>
#include <stdlib.h>
//#include <stdbool.h>
//#include "lwfunc.h"

//Josephus com n pulos

struct lista{
	int info;
	struct lista *prox;
};

typedef struct lista Lista;

int mataAlguem(Lista *l,int saltos);
int *alocaVetorInt(int n);
Lista* listaCria();
Lista* listaInsere(Lista *l, int i);
int *liberaVetorInt(int *p);

int main(void){

	int numero_pessoas, //N�mero de pessoas na roda
		numero_pulos,   //N�mero de pulos para matar o proximo
		casos_teste,    //N�mero de casos de texte
		conta_teste=0,  //Contador de casos
		conta_pessoas,  //Contador de pessoas
		*sobrevivente;

	Lista *pessoas,
		  *p;

	scanf("%d",&casos_teste);
	sobrevivente = alocaVetorInt(casos_teste);

	do{

		p = pessoas = listaCria();


		scanf("%d %d",&numero_pessoas,&numero_pulos);
		for(conta_pessoas = numero_pessoas; conta_pessoas > 0;--conta_pessoas){

			pessoas = listaInsere(pessoas,conta_pessoas);
		}



        for(p = pessoas; p != NULL;p = p->prox){
        	if(p->prox == NULL){
        		p->prox = pessoas;
        		break;
            }
        }

        sobrevivente[conta_teste] = mataAlguem(pessoas,numero_pulos)-1;


		++conta_teste;
	}while(conta_teste < casos_teste);

	for(conta_teste = 0;conta_teste < casos_teste;++conta_teste){

		printf("Case %d: %d\n",conta_teste+1,sobrevivente[conta_teste]);
	}

	sobrevivente = liberaVetorInt(sobrevivente);
    return 0;
}


int mataAlguem(Lista *l,int saltos){
	int i;
	Lista *p = l,*aux;

	do{
		for(i = 0;i < saltos;i++){
			aux = p;
			p = p->prox;
		}

		aux->prox = p->prox;
	}while(p != aux);


	return p->info;
 }

 int *alocaVetorInt(int n){

	int *ponteiro;

	ponteiro = (int*)calloc(n,sizeof(int));

	return ponteiro;
}

Lista* listaCria(){

	return NULL;
}

Lista* listaInsere(Lista *l, int i){

	Lista *novo = (Lista *)malloc(sizeof(Lista));

	novo->info = i;
	novo->prox = l;

	return novo;
}

int *liberaVetorInt(int *p){

	free(p);

	return p;
}
