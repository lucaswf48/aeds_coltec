//#define N 50

/*struct pilha{

	int n;
	float vet[N];
};

typedef struct pilha Pilha;*/

struct lista{

	float info;
	struct lista *prox;
};

typedef struct lista Lista;

struct pilha{

	Lista *prim;
};

typedef struct pilha Pilha;

struct calc{

	char f[21];
	Pilha *p;
};

typedef struct calc Calc;

Pilha *pilha_cria();
void pilha_push(Pilha *p,float v);
float pilha_pop(Pilha *p);
int pilha_vazia(Pilha *p);
void pilha_libera(Pilha *p);
void pilha_imprime(Pilha *p);
Calc *calc_cria(char *formato);
void calc_operando(Calc *c, float v);
void calc_libera(Calc *c);
void calc_operador(Calc *c, char op);