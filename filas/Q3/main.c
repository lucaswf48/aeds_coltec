/*3. Vamos montar um estacionamento! Compramos um terreno que
possui uma entrada e uma saída. Quando chega um novo carro,
este é estacionado no terreno, um atrás do outro. Quando um
carro precisa sair, os carros do terreno são retirados pela saída, dão
uma volta na quadra e são colocados no final da fila pela entrada
do estacionamento. Faça um programa que inclua carros no
estacionamento informando o número da placa e retire carros
usando o identificador (placa). Depois de ter informado a placa,
exiba o estado do estacionamento.
*/

#include <stdio.h>
#include <stdbool.h>
#include "lwfunc.h"

void menu(void);
void adicionaCarro(Fila *f);
int verificaPlaca(char *placaTesta,Fila *f);
void imprimeEstacionamento(Fila *f);
void retiraCarro();

int main(void){

	int escolha;
	Fila *estacionamento = filaCria();
	do{
		menu();
		scanf("%d",&escolha);
		limpaBuffer();
		switch(escolha){
			case 1:
				adicionaCarro(estacionamento);
				break;
			case 2:
			    retiraCarro(estacionamento);
				break;
			case 3:
				imprimeEstacionamento(estacionamento);
				break;
			case 0:
				break;
		}
		continua();
	}while(escolha != 0);

    return 0;
}

void menu(void){

	printf("Digite:\n");
	printf("1 - Para adicionar um carro\n");
	printf("2 - Para retirar um carro\n");
	printf("3 - Para imprimir os carros estacionados\n");
	printf("0 - Para sair do programa\n");
}

void adicionaCarro(Fila *f){

	char *placaTesta;
	printf("Digite a placa do carro:");
	placaTesta = lerStringN();
	if(verificaPlaca(placaTesta,f)){

		filaInsere(f,placaTesta);
	}
	else{
		printf("Placa repitida.!!!!!!!!!!!!!!!!!!!!\n");
	}
}

int verificaPlaca(char *placaTesta,Fila *f){


	Lista *l;
	if(filaVazia(f)){
		return 1;
	}
	else{
		for(l = f->ini;l != NULL;l = l->prox){
			if(strcmp(placaTesta,l->placa) == 0)
				return 0;
		}
		return 1;
	}
}

void imprimeEstacionamento(Fila *f){

	if(filaVazia(f)){
		printf("Estacionamento vazio\n");
	}
	else{
		filaImprime(f);
	}
}

void retiraCarro(Fila *f){

    char *fraseTesta;
    int avalia;
    if(filaVazia(f)){

    	printf("Estacionamento vazio\n");
    }
    else{

    	char *aponta;
    	printf("Digite a placa do carro a ser retirado:");
	    fraseTesta = lerStringN();
	   	avalia = verificaPlaca(fraseTesta,f);
	    if(avalia == 0){

	    	do{
	    		aponta = filaRetira(f);
	    		filaInsere(f,aponta);
	    	}while(strcmp(fraseTesta,f->ini->placa)!=0);
	    	filaRetira(f);
	    }
	    else if(avalia == 1){
	    	printf("Carro não encontrado");
	    }
    }

}
