#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "lwfunc.h"

#define DIM 100

//Se o sistema operacional for linux
#ifdef linux

//Cria a função para limpar o buffer com o __fpurge(stdin)
void limpaBuffer(){

	__fpurge(stdin);
}

//Cria a função que limpa a tela com system("clear")
void limpaTela(){

	system("clear");
}



//Se o sistema operacional for windows
#elif WIN32


//Cria a função para limpar o buffer com o fflush(stdin)
void limpaBuffer(){

	fflush(stdin);
}

//Cria a função que limpa a tela com system("cls")
void limpaTela(){

	system("cls");
}

#endif

void continua(){

    printf("\nAperte \"ENTER\" para continuar");
    limpaBuffer();
    getchar();
    limpaTela();
}
//
char* lerStringN(){

	char *frase = (char*)malloc(sizeof(char));
	int cont = 0;

	do{

		frase[cont] = getchar();
		if(frase[cont] == '\n'){
			frase[cont] = '\0';
			break;
		}
		else{
			++cont;
			frase = (char*)realloc(frase,(cont+1)*sizeof(char));
		}
	}while(1);

	return frase;
}
//
void lerVetorInt(int vet[],int tam){

	int j;
	for(j = 0;j < tam;++j){
		printf("Posição %d:\n",j);
		scanf("%d",&vet[j]);
	}
	printf("\n");
}

void imprimeVetorInt(int vet[],int tam){

	int j;
	for(j = 0;j < tam;++j){
		printf("%d, ",vet[j]);
	}
	printf("\n");
}

//Recebe como parametro o tamanho N do vetor
int *alocaVetorInt(int n){

	int *ponteiro;

	ponteiro = (int*)calloc(n,sizeof(int));

	return ponteiro;
}

int *liberaVetorInt(int *p){

	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Primeiro parametro linha segundo coluna
void lerMatrizInt(int **p,int a, int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;++i){
			printf("Posição [%d] [%d]\n", j,i);
			scanf("%d",&p[j][i]);
		}
	}
}

void imprimeMatrizInt(int **p,int a, int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;++i){
			printf("%d ",p[j][i]);
		}
		printf("\n");
	}
}

int** alocaMatrizInt(int a,int b){

	int **ponteiro,i;

	ponteiro = (int**)calloc(a,sizeof(int*));

	for(i = 0;i < a;++i){

		ponteiro[i] =(int*)calloc(b,sizeof(int));
	}

	return ponteiro;
}

int **liberaMatrizInt(int **p,int a){

	int i;

	for(i = 0;i<a;i++){
		free(p[i]);
	}
	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerVetorFloat(float vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		scanf("%f",&vet[j]);
	}
	printf("\n");
}

void imprimeVetorFloat(float vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		printf("%f, ",vet[j]);
	}
	printf("\n");
}

//Recebe como parametro o tamanho N do vetor
float* alocaVetorFloat(int n){

	float *ponteiro;

	ponteiro =(float*)calloc(n,sizeof(float));

	return ponteiro;
}

float* liberaVetorFloat(float *p){

	free(p);

	return p;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerMatrizFloat(float **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			scanf("%f",&p[j][i]);
		}
	}
}

void imprimeMatrizFloat(float **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			printf("%f",p[j][i]);
		}
		printf("\n");
	}
}

float** alocaMatrizFloat(int a,int b){

	float **ponteiro;
	int i;

	ponteiro = (float**)calloc(a,sizeof(float*));

	for(i = 0; i < a;++i){
		ponteiro[i] = (float*)calloc(b,sizeof(float));
	}

	return ponteiro;
}

float** liberaMatrizFloat(float **p, int a){

	int i;

	for(i = 0;i<a;i++){
		free(p[i]);
	}
	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerVetorDouble(double vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		scanf("%lf",&vet[j]);
	}
	printf("\n");
}

void imprimeVetorDouble(double vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		printf("%lf, ",vet[j]);
	}
	printf("\n");
}

//Recebe como parametro o tamanho N do vetor
double* alocaVetorDouble(int n){

	double *ponteiro;

	ponteiro =(double*)calloc(n,sizeof(double));

	return ponteiro;
}

double* liberaVetorDouble(double *p){

	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerMatrizDouble(double **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			scanf("%lf",&p[j][i]);
		}
	}
}

void imprimeMatrizDouble(double **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			printf("%lf",p[j][i]);
		}
		printf("\n");
	}
}

double** alocaMatrizDouble(int a,int b){

	double **ponteiro;
	int i;

	ponteiro = (double**)calloc(a,sizeof(double*));

	for(i = 0; i < a;++i){
		ponteiro[i] = (double*)calloc(b,sizeof(double));
	}

	return ponteiro;
}

double** liberaMatrizDouble(double **p,int a){


	int i;

	for(i = 0;i < a;++i){
		free(p[i]);
	}


	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void bubbleSort(int vet[],int t){
	int b,a,al;
	for (a = 0; a <= t;a++){
		 for(b = 0; b <= t;b++){
			 if (vet[a] < vet[b]){
			 al = vet[a];
			 vet[a] = vet[b];
			 vet[b] = al;
			 }
		 }
	 }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

int partition(int a[],int l, int r){
	int pivot,i,j,aux;
	pivot = a[l];
	i=l;
	j=r+1;
	while(1){
		do ++i;while(a[i] <= pivot && i <= r);
		do --j;while(a[j] > pivot);
		if(i >= j)break;
		aux = a[i];
		a[i]=a[j];
		a[j]= aux;
	}
	aux= a[l];
	a[l]= a[j];
	a[j]= aux;
	return j;
}

void quickSort(int a[],int l,int r){
	int j;
	if(l < r){
		j=partition(a,l,r);
		quickSort(a,l,j-1);
		quickSort(a,j+1,r);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void merge(int arr[], int l, int m, int r){

    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;


    int L[n1], R[n2];


    for(i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for(j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];


    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2){
        if (L[i] <= R[j]){

            arr[k] = L[i];
            i++;
        }
        else{

            arr[k] = R[j];
            j++;
        }
        k++;
    }


    while (i < n1){
        arr[k] = L[i];
        i++;
        k++;
    }


    while (j < n2){
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int arr[], int l, int r){
    if (l < r){

        int m = l+(r-l)/2;
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);
        merge(arr, l, m, r);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Função que retorna a hora atual no formato hora/minuto/segundo, na forma de uma string
char* escreveHoraFormatada(){

	int segundo,
		minuto,
		hora;

	char segundoS[DIM],
		minutoS[DIM],
		horaS[DIM],
		*horaformatado;

	horaformatado = (char *)malloc(DIM*sizeof(char));

	time_t hora_atual = time (NULL);
	struct tm *t = localtime (&hora_atual);

	segundo = t->tm_sec;
	minuto = t->tm_min;
	hora = t->tm_hour;

	sprintf(horaS,"%02d",hora);
	sprintf(minutoS,"%02d",minuto);
	sprintf(segundoS,"%02d",segundo);
	sprintf(horaformatado,"%s:%s:%s",horaS,minutoS,segundoS);

	return horaformatado;
}

//Função que retorna a hora atual no formato dd/mm/aaaa, na forma de uma string
char* escreveDataFormatada(){


	int dia,
		mes,
		ano;

	char diaS[DIM],
		mesS[DIM],
		anoS[DIM],
		*dataformatado;

	dataformatado = (char *)malloc(DIM*sizeof(char));

	time_t hora_atual = time (NULL);
	struct tm *t = localtime (&hora_atual);

	dia = t->tm_mday;
	mes = t->tm_mon + 1;
	ano = t->tm_year + 1900;

	sprintf(diaS,"%02d",dia);
	sprintf(mesS,"%02d",mes);
	sprintf(anoS,"%02d",ano);
	sprintf(dataformatado,"%s/%s/%s",diaS,mesS,anoS);

	return dataformatado;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Fila *filaCria(){
	Fila *f = (Fila *)malloc(sizeof(Fila));
	f->ini = f->fim = NULL;
	return f;
}

void filaInsere(Fila *f,char *v){


	Lista *n = (Lista *)malloc(sizeof(Lista));
	n->placa = (char*)malloc(strlen(v)*sizeof(char));
	strcpy(n->placa,v);
	n->prox = NULL;
	if(f->fim != NULL)
		f->fim->prox = n;
	else
		f->ini = n;
	f->fim = n;

}

char* filaRetira(Fila *f){

	Lista *t;
	char *v;
	if(filaVazia(f)){
		printf("Vazia\n");
		exit(1);
	}
	t = f->ini;
	v = (char*)malloc(strlen(t->placa)*sizeof(char));
	strcpy(v,t->placa);
	f->ini = t->prox;
	if(f->ini ==NULL)
		f->fim = NULL;
	free(t);
	return v;
}

int filaVazia(Fila *f){

	return (f->ini == NULL);
}

void filaLibera(Fila *f){

	Lista *q = f->ini;
	while(q != NULL){

		Lista *t = q->prox;
		free(q);
		q = t;
	}
	free(f);
}
void filaImprime(Fila *f){
	Lista *q;
	for(q = f->ini; q!= NULL; q = q->prox)
		printf("%s \n", q->placa);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






