//#include <stdbool.h>
/*==========  Listas Encadeadas  ==========*/

struct lista{
	//int info;
	char *placa;
	struct lista *prox;
};

typedef struct lista Lista;

struct lista2{
	//int info;
	struct lista2 *ant;
	struct lista2 *prox;
};

typedef struct lista2 Lista2;

///////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Fila  ==========*/

struct fila{

	Lista *ini;
	Lista *fim;
};

typedef struct fila Fila;
///////////////////////////////////////////////////////////////////////////////////////////////////////////


/*==========  Pilha  ==========*/

struct pilha{

	Lista *prim;
};

typedef struct pilha Pilha;

struct calc{

	char f[21];
	Pilha *p;
};

typedef struct calc Calc;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Arvores  ==========*/

struct arv {
 char info;
 struct arv* esq;
 struct arv* dir;
};

typedef struct arv Arv;

struct arvvar{
	char info;
	struct arvvar *prim;
	struct arvvar *prox;
};

typedef struct arvvar ArvVar;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Arvores AVL ==========*/

typedef struct no *pno;

typedef struct no{

	int bal;
	int info;
	pno dir,esq;
}no;

typedef pno tree;

tree raiz;

char* lerStringN();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*==========  Vetores de inteiros  ==========*/


void lerVetorInt(int vet[],int tam);
void imprimeVetorInt(int vet[],int tam);
int *alocaVetorInt(int n);
int *liberaInt(int *p);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*==========  Matrizes de inteiros  ==========*/



void lerMatrizInt(int **p,int a, int b);
void imprimeMatrizInt(int **p,int a, int b);
int** alocaMatrizInt(int a,int b);
int** liberaMatrizInt(int **p,int a);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Vetores de "floats"  ==========*/


void lerVetorFloat(float vet[],int tam);
void imprimeVetorFloat(float vet[],int tam);
float* alocaVetorFloat(int n);
float* liberaVetorFloat(float *p);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Matrizes de "floats"  ==========*/


void lerMatrizFloat(float **p,int a,int b);
void imprimeMatrizFloat(float **p,int a,int b);
float** alocaMatrizFloat(int a,int b);
float** liberaMatrizFloat(float **p, int a);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Vetores de "doubles"  ==========*/


void lerVetorDouble(double vet[],int tam);
void imprimeVetorDouble(double vet[],int tam);
double* alocaVetorDouble(int n);
double* liberaVetorDouble(double *p);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Matrizes de "doubles"  ==========*/


void lerMatrizDouble(double **p,int a,int b);
void imprimeMatrizDouble(double **p,int a,int b);
double** alocaMatrizDouble(int a,int b);
double** liberaMatrizsDouble(double **p,int a);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Ordenação de vetor: Bubbles Sort  ==========*/


void bubbleSort(int vet[],int t);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Ordenação de vetor: Quick Sort  ==========*/


int partition(int a[],int l, int r);
void quickSort(int a[],int l,int r);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Ordenação de vetor: Merge Sort  ==========*/


void merge(int arr[], int l, int m, int r);
void mergeSort(int arr[], int l, int r);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Data e Hora do sistema  ==========*/



char* escreveHoraFormatada();
char* escreveDataFormatada();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Limpar tela e limpar buffer  ==========*/


void limpaTela();
void limpaBuffer();
void continua();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Fila  ==========*/

Fila *filaCria();
void filaInsere(Fila *f, char *v);
char* filaRetira(Fila *f);
int filaVazia(Fila *f);
void filaLibera(Fila *f);
int filaIncr(int i);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////







