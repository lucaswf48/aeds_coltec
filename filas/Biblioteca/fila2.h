
struct no2 {
 float info;
 struct no2* ant;
 struct no2* prox;
};
typedef struct no2 No2;

struct fila2 {
 No2* ini;
 No2* fim;
};


typedef struct fila2 Fila2;


Fila2* cria (void);
void insere_ini (Fila2* f, float v);
void insere_fim (Fila2* f, float v);
float retira_ini (Fila2* f);
float retira_fim (Fila2* f);
int vazia (Fila2* f);
void libera (Fila2* f);