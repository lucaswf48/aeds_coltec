#define N 100

/*struct fila{

	int n;
	int ini;
	float vet[N];
}*/

struct lista{
	float info;
	struct lista *prox;
}

struct fila{

	Lista *ini;
	Lista *fim;
}

typedef struct lista Lista;
typedef struct fila Fila;

Fila *fila_cria();
void fila_insere(Fila *f, float v);
float fila_retira(Fila *f);
int fila_vazia(Fila *f);
void fila_libera(Fila *f);
static int incr(int i);
