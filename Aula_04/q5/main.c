#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"
#define tamanho 101



int main(){

	int r;
	char s1[tamanho];

	printf("Digite uma frase:\n");
	fgets(s1,100,stdin);

	r = palindromo(s1);

	if (r == 1)
	{
		printf("A frase é palíndrona.\n");
	}
	else{
		printf("A frase não é palíndrona.\n");
	}

	printf("%s\n", s1);
	return 0;
}

int palindromo(char s1[]){

	int cont,x,i,a;
	Pilha *p1 = pilha_cria();

	for (cont = 0; s1[cont] != '\0';cont++)
	{
		if ((s1[cont] < 65 || s1[cont] > 90)&&(s1[cont]< 97 || s1[cont] > 122)){			
			for (x = cont;s1[x] != '\0';++x){
					s1[x] = s1[x + 1];
				}
			cont--;
		}
	}

	s1[cont] = '\0';

	for(cont = 0;s1[cont] != '\0';++cont){
		s1[cont] = tolower(s1[cont]);
    }


    i = strlen(s1);
    
    for(cont = 0;cont < i;cont++){
    	pilha_push(p1,s1[cont]);
    }
    for(cont = 0;cont < i;cont++){
    	
    	a = pilha_pop(p1);
    	
    	if(a != s1[cont]){
    		
    		return 0;
    	}
    }

    return 1;
}
