#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"


/*Pilha *pilha_cria(){

	Pilha *p = (Pilha*)malloc(sizeof(Pilha));
	p->n = 0; //Inicializa com zero elementos

	return p;
}

void pilha_push(Pilha *p,float v){

	if(p->n==N){

		printf("Capacidade da pilha estourou.");
		exit(1);
	}

	p->vet[p->n] = v;
	p->n++;
}

float pilha_pop(Pilha *p){

	float v;
	if(pilha_vazia(p)){
		printf("Pilha vazia!\n");
		exit(1);
	}
	//Retira elemento do topo
	v = p->vet[p->n-1];
	p->n--;
	return v;
}

int pilha_vazia(Pilha *p){

	return (p->n == 0);
}

void pilha_libera(Pilha *p){

	free(p);
}

void pilha_imprime(Pilha *p){

	int i;
	for(i = p->n-1;i >= 0;i--)
		printf("%f ",p->vet[i]);
}*/
//////////////////////////////////////////////////////////////////////////////////////////////

Pilha *pilha_cria(){

	Pilha *p = (Pilha*)malloc(sizeof(Pilha));
	p->prim = NULL;
	return p;
}

void pilha_push(Pilha *p,float v){

	Lista *n = (Lista *)malloc(sizeof(Lista));
	n->info = v;
	n->prox = p->prim;
	p->prim = n;
}

float pilha_pop(Pilha *p){

	Lista *t;
	float v;
	if(pilha_vazia(p)){

		//printf("Pilha vazia\n");
		//exit(1);
		return -1;
	}
	t = p->prim;
	v = t->info;
	p->prim = t->prox;
	free(t);
	return v;
}

int pilha_vazia(Pilha *p){
	return (p->prim == NULL);
}

void pilha_libera(Pilha *p){

	Lista *q = p->prim;
	while(q != NULL){
		Lista *t = q->prox;
		free(q);
		q = t;
	}

	free(q);
}


void pilha_imprime(Pilha *p){

	Lista *q;
	for(q = p->prim;q != NULL;q = q->prox)
		printf("%f ",q->info);
	printf("\n");
	printf("\n");
}

/////////////////////////////////////////////////////////////////////////////////////

Calc *calc_cria(char *formato){

	Calc *c = (Calc*)malloc(sizeof(Calc));
	strcpy(c->f, formato);
	c->p = pilha_cria(); 
	return c;
}

void calc_operando(Calc *c, float v){
	
	pilha_push(c->p, v);
	printf(c->f, v); 
}

void calc_libera(Calc *c){
	
	pilha_libera(c->p);
	free(c);
}

void calc_operador(Calc *c, char op){
	float v1, v2, v;
	
	if (pilha_vazia(c->p))
		v2 = 0.0;
	else
		v2 = pilha_pop(c->p);
	if (pilha_vazia(c->p))
		v1 = 0.0;
	else
		v1 = pilha_pop(c->p);
	
	switch(op){

		case '+': 
			v = v1 + v2; 
			break;
		case '-': 
			v = v1 - v2; 
			break;
		case '*': 
			v = v1 * v2; 
			break;
		case '/': 
			v = v1 / v2; 
			break;
	}
	
	pilha_push(c->p, v);
	
	printf(c->f, v);
}