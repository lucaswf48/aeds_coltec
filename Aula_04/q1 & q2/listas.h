/*definindo lista encadeada*/
struct lista_int{
	int info;
	struct lista_int *prox;
};

typedef struct lista_int Lista_int;


struct lista_float{

	float info;
	struct lista_float *prox;
};

typedef struct lista_float Lista_float;
	
/*definindo lista duplamente encadeada*/
struct lista2_int{
	int info;
	struct lista2_int *ant;
	struct lista2_int *prox;
};



typedef struct lista2_int Lista2_int;

struct lista2_float{

	float info;
	struct lista2_float *ant;
	struct lista2_float *prox;
};

typedef struct lista2_float Lista2_float;


/*Funcoes de lista encadeada*/

Lista_int* lst_cria();

Lista_int* lst_insere(Lista_int *l, int i);

void lst_imprime_rec(Lista_int *l);

void lst_imprime(Lista_int *l);

int lst_vazia(Lista_int *l);

Lista_int* lst_busca(Lista_int *l, int v);

Lista_int *lst_retira(Lista_int *l, int v);

void lst_libera(Lista_int *l);
void lst_libera_rec(Lista_int *l);
Lista_int *lst_intercala(Lista_int *l1,Lista_int *l2);
Lista_int *lst_insere_ordenado(Lista_int *l, int v);

int lst_igual(Lista_int *l1, Lista_int *l2);

/*lista_ints circulares*/
void lcirc_imprime(Lista_int *l); 

/*listas duplamente encadeadas*/
Lista2_int *lst2_insere(Lista2_int *l, int v);

Lista2_int *lst2_busca(Lista2_int *l, int v);

Lista2_int *lst2_retira(Lista2_int *l, int v);


