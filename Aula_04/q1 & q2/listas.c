#include <stdio.h>
#include <stdlib.h>
#include "listas.h"

/*Funcoes de lista encadeada*/

Lista_int* lst_cria(){

	return NULL;
}

Lista_int* lst_insere(Lista_int *l, int i){

	Lista_int *novo = (Lista_int *)malloc(sizeof(Lista_int));

	novo->info = i;
	novo->prox = l;

	return novo;
}

void lst_imprime_rec(Lista_int *l){

	if(l == NULL){
		return;
	}
	else{
		printf("Info %d \n",l->info);
		lst_imprime_rec(l->prox);
	}


}


void lst_imprime(Lista_int *l){

	Lista_int *p;

	for(p = l;p != NULL;p = p->prox){
		printf("Info %d \n", p->info);
		//printf("prox %p \n", p->prox);
	}
}

int lst_vazia(Lista_int *l){

	return (l == NULL);
}

Lista_int* lst_busca(Lista_int *l, int v){

	Lista_int *p;

	for(p = l; p != NULL; p = p->prox){
		if(p->info == v){
			return p;
		}
	}

	return NULL;
}

Lista_int *lst_retira(Lista_int *l, int v){

	Lista_int *ant = NULL;
	Lista_int *p = l;

	while(p != NULL && p->info != v){

		ant = p;
		p = p->prox;
	}

	if(p == NULL)
		return l;

	if(ant == NULL)
		l = p->prox;
	else
		ant->prox = p->prox;

	free(p);

	return l;
}

Lista_int *lst_retira_rec(Lista_int *l, int v){

	if(l == NULL){
		return;
	}
	else{
		if(l->info==v){
			Lista_int *p;
			p = l;
			l=l->prox;
		}
		else{
				l->prox = lst_retira_rec(l->prox,v);
		}
	}
	return l;
}

void lst_libera(Lista_int *l){

	Lista_int *p = l;
	while(p != NULL){

		Lista_int *t = p->prox;
		free(p);
		p = t;
	}
}

void lst_libera_rec(Lista_int *l){

	if(l == NULL){
		return;
	}
	else{
		
		lst_libera_rec(l->prox);
		free(l);
	}

}

Lista_int *lst_insere_ordenado(Lista_int *l, int v){

	Lista_int *novo;
	Lista_int *ant = NULL;
	Lista_int *p = l;

	while(p != NULL && p->info < v){
		ant = p;
		p = p->prox;
	}

	novo = (Lista_int*)malloc(sizeof(Lista_int));
	novo->info = v;

	if(ant == NULL){
		novo->prox = l;
		l = novo;
	}
	else{
		novo->prox = ant->prox;
		ant->prox = novo;
	}
	return l;
}

int lst_igual(Lista_int *l1, Lista_int *l2){

	Lista_int *p = l1, *p1=l2;


	while(1){
	
		if(p->info!=p1->info){

			return 0;
		}
		if(p1->prox == NULL && p->prox==NULL){
			break;
		}
		p = p->prox;
		p1 = p1->prox;
	}

	return 1;
}

Lista_int *lst_intercala(Lista_int *l1,Lista_int *l2){

	Lista_int *p1 = l1,*p2 = l2,*d1,*d2;
	int i=0;

	while(p1 != NULL || p2 != NULL){


		if(p1->prox == NULL && p2->prox != NULL){
			if(i==0){
			p1->prox=l2;
			break;
			}
			else{
				p1->prox=d2;
				break;
			}	
		}
		if(p1->prox != NULL && p2->prox == NULL){
			
			if(i==0){
				d1=p1->prox;
				p1->prox=p2;
				p2->prox=d1;
				break;
			}
			else{
				d1=p1->prox;
				p1->prox=d2;
				p1->prox->prox=d1;
				break;
			}		
		}
		d1=p1->prox;
		d2=p2->prox;
		p1->prox=p2;
		p1->prox->prox=d1;
		p2=d2;
		p1=d1;		
		i++;
		
	}
			  
	

	return l1;
}



/*lista_ints circulares*/
void lcirc_imprime(Lista_int *l){

	Lista_int *p = l;
	if(p)
		do{
			printf("%d\n", p->info);
			p=p->prox;
		}while(p!=l);
}

/*lista_ints duplamente encadeadas*/
Lista2_int *lst2_insere_dupla(Lista2_int *l, int v){

	Lista2_int *novo = (Lista2_int *)malloc(sizeof(Lista2_int));
	Lista2_int *daw;
	novo->info = v;
	novo->prox = l;
	daw=l->ant;
	daw->prox=novo;
	novo->ant = daw;
	if(l != NULL){
		l->ant = novo;
	}

	return novo;
}

Lista2_int *lst2_insere(Lista2_int *l, int v){

	Lista2_int *novo = (Lista2_int *)malloc(sizeof(Lista2_int));
	novo->info = v;
	novo->prox = l;
	novo->ant = NULL;
	if(l != NULL){
		l->ant = novo;
	}

	return novo;
}




Lista2_int *lst2_busca(Lista2_int *l, int v){

	Lista2_int *p;

	for(p = l;p!= NULL;p=p->prox){
			if(p->info == v){
				return p;
			}
	}

	return NULL;
}

Lista2_int *lst2_retira(Lista2_int *l, int v){

	Lista2_int *p = lst2_busca(l,v);
	if(p == NULL){
		return l;
	}

	if(l==p)
		l = p->prox;
	else
		p->ant->prox = p->prox;

	if(p->prox != NULL)
		p->prox->ant = p->ant;

	free(p);

	return l;
}


