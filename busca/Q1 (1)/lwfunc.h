/*==========  Listas Encadeadas  ==========*/

struct lista{
	int info;
	struct lista *prox;
};

typedef struct lista Lista;

struct lista2{
	int info;
	struct lista2 *ant;
	struct lista2 *prox;
};

typedef struct lista2 Lista2;

///////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Fila  ==========*/

struct fila{

	Lista *ini;
	Lista *fim;
};

typedef struct fila Fila;
///////////////////////////////////////////////////////////////////////////////////////////////////////////


/*==========  Pilha  ==========*/

struct pilha{

	Lista *prim;
};

typedef struct pilha Pilha;

struct calc{

	char f[21];
	Pilha *p;
};

typedef struct calc Calc;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Arvores  ==========*/

struct arv {
 char info;
 struct arv* esq;
 struct arv* dir;
};

typedef struct arv Arv;

struct arvvar{
	char info;
	struct arvvar *prim;
	struct arvvar *prox;
};

typedef struct arvvar ArvVar;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Arvores AVL ==========*/

typedef struct no *pno;

typedef struct no{

	int bal;
	int info;
	pno dir,esq;
}no;

typedef pno tree;

tree raiz;

char* lerStringN();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct grafo{

	bool orientado;
	bool ponderado;
	int numero_vertices;
	int **matriz;

}Grafo;


typedef struct grafolista{

	//bool orientado;
	bool ponderado;
	int numero_vertices;
	Lista **vet_lista;

}GrafoLista;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*==========  Vetores de inteiros  ==========*/


void lerVetorInt(int vet[],int tam);
void imprimeVetorInt(int vet[],int tam);
int *alocaVetorInt(int n);
int *liberaInt(int *p);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*==========  Matrizes de inteiros  ==========*/



void lerMatrizInt(int **p,int a, int b);
void imprimeMatrizInt(int **p,int a, int b);
int** alocaMatrizInt(int a,int b);
int** liberaMatrizInt(int **p,int a);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Vetores de "floats"  ==========*/


void lerVetorFloat(float vet[],int tam);
void imprimeVetorFloat(float vet[],int tam);
float* alocaVetorFloat(int n);
float* liberaVetorFloat(float *p);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Matrizes de "floats"  ==========*/


void lerMatrizFloat(float **p,int a,int b);
void imprimeMatrizFloat(float **p,int a,int b);
float** alocaMatrizFloat(int a,int b);
float** liberaMatrizFloat(float **p, int a);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Vetores de "doubles"  ==========*/


void lerVetorDouble(double vet[],int tam);
void imprimeVetorDouble(double vet[],int tam);
double* alocaVetorDouble(int n);
double* liberaVetorDouble(double *p);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Matrizes de "doubles"  ==========*/


void lerMatrizDouble(double **p,int a,int b);
void imprimeMatrizDouble(double **p,int a,int b);
double** alocaMatrizDouble(int a,int b);
double** liberaMatrizsDouble(double **p,int a);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Ordenação de vetor: Bubbles Sort  ==========*/


void bubbleSort(int vet[],int t);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Ordenação de vetor: Quick Sort  ==========*/


int partition(int a[],int l, int r);
void quickSort(int a[],int l,int r);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Ordenação de vetor: Merge Sort  ==========*/


void merge(int arr[], int l, int m, int r);
void mergeSort(int arr[], int l, int r);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Data e Hora do sistema  ==========*/



char* escreveHoraFormatada();
char* escreveDataFormatada();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Limpar tela e limpar buffer  ==========*/


void limpaTela();
void limpaBuffer();
void continua();

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Listas Encadeadas  ==========*/

Lista* listaCria();
Lista* listaInsere(Lista *l, int i);
void listaImprimeRec(Lista *l);
void listaImprime(Lista *l);
int listaVazia(Lista *l);
Lista* listaBusca(Lista *l, int v);
Lista *listaRetira(Lista *l, int v);
void listaLibera(Lista *l);
void listaLiberaRec(Lista *l);
Lista *listaIntercala(Lista *l1,Lista *l2);
Lista *listaInsereOrdenado(Lista *l, int v);
int listaIgual(Lista *l1, Lista *l2);
void lcircImprime(Lista *l);
void listaCircularLibera(Lista *l);
Lista2 *lista2Insere(Lista2 *l, int v);
Lista2 *lista2Busca(Lista2 *l, int v);
Lista2 *lista2Retira(Lista2 *l, int v);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Fila  ==========*/

Fila *filaCria();
void filaInsere(Fila *f, float v);
float filaRetira(Fila *f);
int filaVazia(Fila *f);
void filaLibera(Fila *f);
int filaIncr(int i);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Pilha  ==========*/

Pilha *pilhaCria();
void pilhaPush(Pilha *p,float v);
float pilhaPop(Pilha *p);
int pilhaVazia(Pilha *p);
void pilhaLibera(Pilha *p);
void pilhaImprime(Pilha *p);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

Calc *calc_cria(char *formato);
void calc_operando(Calc *c, float v);
void calc_libera(Calc *c);
void calc_operador(Calc *c, char op);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*==========  Arvores  ==========*/

Arv* arvoreInicializa(void);
Arv* arvoreCria(char c, Arv* sae, Arv* sad);
int arvoreVazia(Arv* a);
void arvoreImprimePre (Arv* a);
void arvoreImprimeSim(Arv* a);
void arvoreImprimePos(Arv* a);
Arv* arvoreInsere(Arv *a,char carac);
Arv* arvoreLibera (Arv* a);
int arvoreBusca (Arv* a, char c);
int arvoreMax2 (int a ,int b);
void arvoreContaFolhasFilhos(Arv *p,int *folhas,int *filho1,int *filho2);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

int arv_altura(Arv *a);
ArvVar *arvv_cria(char c);
void arvv_insere(ArvVar *a,ArvVar *sa);
void arvv_imprime(ArvVar *a);
int arvv_pertence(ArvVar *a, char c);
void arvv_libera(ArvVar *a);
int arvv_altura(ArvVar *a);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void arvoreAvlRotacionaDireita(pno p);
void arvoreAvlRotacionaEsquerda(pno p);
void arvoreAvlRotacionaEsquerdaDireita(pno p);
void arvoreAvlRotacionaDireitaEsquerda(pno p);
void arvoreAvlInsere(int x, pno p,bool kank);
void arvoreAvlAloca(pno p, int x);
void arvoreAvlCaso1(pno p);
void arvoreAvlCaso2(pno p);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

Grafo* alocaGrafo();
void alocaMatrizAdjacente(Grafo *g, int n,bool ori,bool pon);
void imprimeMatrizAdjacente(Grafo *g);
Grafo* insereMarizAdjacente(Grafo *g, int vertice1, int vertice2);
Grafo* insereMarizAdjacente2(Grafo *g, int vertice1, int vertice2,int peso);
bool avaliaVertices(Grafo *g,int a,int b);
void imprimeCaracteristicas(Grafo *g);

////////////////////////////////////////////////////////////////////////////////////////////////////////////
void grauVertice(Grafo *g);
GrafoLista* alocaGrafoLista();
void criaListaAdjacencias(GrafoLista *gl, int n,bool pon);
void insereListaAdjacencias(GrafoLista *gl,int a,int b);
void imprimeListaAdjacencias(GrafoLista *gl);

/////////////////////////////////////////////////////

int buscaEmLargura(Grafo *g,int inicio);
Grafo* inserir(Grafo *g);
void imprimir(Grafo *g);
Grafo* remover();
Grafo* solucao(Grafo *g);
Grafo* lerArquivoURI();
Grafo* criaLigacao(Grafo *g);
Grafo* criaNovaRede(Grafo *g);
Grafo* alocaRede(Grafo *g);
Grafo* retiraAresta(Grafo *g);

int find(int i,int v[]);
int uni(int i,int j,int v[]);
Grafo* Kruskal(Grafo *g);


