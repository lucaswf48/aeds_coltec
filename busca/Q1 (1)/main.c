#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "lwfunc.h"


//Lista encadeada
typedef struct contato{

	//Nome sem limite de caracteres
	char *nome;

	//Oito digitos
	char telefone[9];

	//Endereço sem limite de caracteres
	char *endereco;

	struct contato* esq;
 	struct contato* dir;

}Contato;

void menuPrincipal(void);
void chamaFuncao(int op,Contato *c);
Contato *insereContato(Contato *c);;
Contato* contatoInsere(Contato *c,char *nombre, char *tel, char *address);
void imprimeContato(Contato *c);
int contatoVazia(Contato *c);
Contato *buscaBinaria(Contato *c,char *nombre);

int main(){

	int opcao;

	Contato *agenda = NULL;

	printf("Insira você como o primeiro contato da sua agenda.\n");
	agenda = insereContato(agenda);

	limpaTela();
	do{

		menuPrincipal();
		scanf("%d",&opcao);
		limpaBuffer();

		chamaFuncao(opcao,agenda);

		continua();
	}while(opcao != 0);

	return 0;
}



void menuPrincipal(void){

	printf("Digite:\n");
	printf("1 - Para adicionar um novo contato\n");
	printf("2 - Para retirar um contato\n");
	printf("3 - Para buscar um contato\n");
	printf("4 - Para imprimir todos os contatos\n");
	printf("0 - Para sair do programa\n");
}

void chamaFuncao(int op,Contato *c){

	Contato *daw =NULL;
	char *knak;

	switch(op){
		case 0:
			printf("Saindo do programa\n");
			break;
		case 1:
			c = insereContato(c);
			break;
		case 2:
			
			break;
		case 3:
		printf("Digite um nome: ");
			knak = lerStringN();
			daw = buscaBinaria(c,knak);
			if(daw != NULL)
				printf("Nome: %s\nTelefone: %s\nEndereço:%s\n",daw->nome,daw->telefone,daw->endereco);
			else
				printf("Contato não encontrado\n");
			break;
		case 4:
			imprimeContato(c);
			break;

	}
}


Contato *insereContato(Contato *c){

	char *aponta_nome;
	char  aponta_telefone[9];
	char *aponta_endereco;

	printf("Nome: ");

	aponta_nome = lerStringN();

	printf("Telefone(8 digitos): ");

	scanf("%s",aponta_telefone);
	limpaBuffer();

	printf("Endereço: ");

	aponta_endereco = lerStringN();
	c = contatoInsere(c,aponta_nome,aponta_telefone,aponta_endereco);

	return c;

}

bool colocaNome(Contato *c,char *nombre){

	int i;

	for(i = 0; nombre[i] != '\0'; ++i){

		if(nombre[i] < c->nome[i])
			return false;
		else if(nombre[i] > c->nome[i])
			return true;
	}

	return false;
}


Contato *buscaBinaria(Contato *c,char *nombre){

	if( c == NULL){
		return NULL;
	}
	else if(strcmp(c->nome,nombre)==0){
		return c;
	}
	else if(!colocaNome(c,nombre)){
		return buscaBinaria(c->esq,nombre);
	}
	else if(colocaNome(c,nombre)){
		
		return buscaBinaria(c->dir,nombre);
	}
	else{
		return c;
	}
}


Contato* contatoInsere(Contato *c,char *nombre, char *tel, char *address){

	if(c == NULL){

		c = (Contato*) malloc(sizeof(Contato));
		c->nome = (char*)malloc(strlen(nombre)*sizeof(char));
		strcpy(c->nome,nombre);

		c->endereco = (char*)malloc(strlen(address)*sizeof(char));
		strcpy(c->endereco,address);
		strcpy(c->telefone,tel);
		c->esq = c->dir = NULL;
	}
	else if(!colocaNome(c,nombre))
		c->esq = contatoInsere(c->esq,nombre,tel,address);
	else
		c->dir = contatoInsere(c->dir,nombre,tel,address);
	return c;
}

void imprimeContato(Contato *c){

	if (!contatoVazia(c)){
		printf("Nome: %s\nTelefone: %s\nEndereço: %s\n\n", c->nome,c->telefone,c->endereco); /* mostra raiz */
		imprimeContato(c->esq); /* mostra sae */
		imprimeContato(c->dir); /* mostra sad */
	}	
}

int contatoVazia(Contato *c){

	return c == NULL;
}