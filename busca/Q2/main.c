#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "lwfunc.h"

#define DIM 23


typedef struct listaContato{

	char *nome;
	char *telefone;
	char *endereco;

	struct listaContato *prox;

}ListaContato;


ListaContato** alocaAgenda(ListaContato **l,int n);
int hash (char *key, int tam);
void menu1();
ListaContato** insereAgenda(ListaContato **l);
ListaContato**  opcao(ListaContato **l, int op);
ListaContato* listaAgenda(ListaContato *l,char *nome, char *telefone, char *endereco);
void imprimeListaAgenda(ListaContato **l,int n);
void buscaAgenda(ListaContato **l,int n);
void imprimiAgenda(ListaContato *l,char *nome);
ListaContato** ritiraAgenda(ListaContato **l);
ListaContato *listadawRetira(ListaContato *l, char *nome );
ListaContato** lerArquivo(ListaContato **l,int n);

int main(){


	int escolha;

	ListaContato **agenda;

	limpaTela();

	agenda = alocaAgenda(agenda,DIM);

	

	do{

		menu1();
		scanf("%d",&escolha);
		limpaBuffer();
		agenda = opcao(agenda,escolha);


		continua();

	}while(escolha != 0);

}


ListaContato** opcao(ListaContato **l, int op){

	switch(op){
		case 1:
			l = insereAgenda(l);
			break;
		case 2:
			l = ritiraAgenda(l);
			break;
		case 3:
			buscaAgenda(l,DIM);
			break;
		case 4:
			imprimeListaAgenda(l,DIM);
			break;
		case 5:
			l = lerArquivo(l,DIM);
		case 0:
			break;
	}

	return l;
}

ListaContato** lerArquivo(ListaContato **l,int n){

	FILE *f;

	char nome[50];
	char *daw;
	char *semi;

	int i;

	f = fopen("semi-kank.txt","r");



	do{

		fscanf(f,"%s",nome);
			
		

		nome[strlen(nome)] = '\0';
		

		daw = nome;

		i = hash(daw,23);


		l[i] = listaAgenda(l[i],daw,daw,daw);
		
		

	}while(!feof(f));

	return l;

}



ListaContato** ritiraAgenda(ListaContato **l){

	char *nome;
	int i = 0;

	printf("Nome: ");
	nome = lerStringN();

	i = hash(nome,DIM);


	l[i] = listadawRetira(l[i],nome);

	return l;

}

ListaContato *listadawRetira(ListaContato *l, char *nome ){

	ListaContato *ant = NULL;
	ListaContato *p = l;

	while(p != NULL && strcmp(nome,p->nome)){

		ant = p;
		p = p->prox;
	}

	if(p == NULL)
		return l;

	if(ant == NULL)
		l = p->prox;
	else
		ant->prox = p->prox;

	free(p);

	return l;
}

void buscaAgenda(ListaContato **l,int n){

	char *nome;
	int i = 0;

	printf("Nome: ");
	nome = lerStringN();

	i = hash(nome,DIM);

	

	imprimiAgenda(l[i],nome);

}


void imprimiAgenda(ListaContato *l,char *nome){

	ListaContato *p;

	for(p = l;p != NULL;p = p->prox){
		
	
			if(strcmp(nome,p->nome) == 0){
				
				printf("Nome: %s \n", p->nome);
				printf("Telefone: %s \n", p->telefone);
				printf("Endereco: %s \n", p->endereco);	
			}
	}

}

void imprimeListaAgenda(ListaContato **l,int n){

	int a;

	for(a=0;a<n;++a){
		ListaContato *p;
		for(p = l[a];p != NULL;p = p->prox){
			printf("Nome: %s \n", p->nome);
			printf("Telefone: %s \n", p->telefone);
			printf("Endereco: %s \n\n", p->endereco);	
		}
		printf("\n");
	}
}


ListaContato** insereAgenda(ListaContato **l){

	int i = 0;
	char *nome,*telefone,* endereco;

	printf("Nome: ");
	nome = lerStringN();

	printf("Telefone: ");
	telefone = lerStringN();
	
	printf("Endereço: ");
	endereco = lerStringN();

	i = hash(nome,DIM);
	

	l[i] = listaAgenda(l[i],nome,telefone,endereco);

	return l;

}

ListaContato* listaAgenda(ListaContato *l,char *nome, char *telefone, char *endereco){

	ListaContato *novo = (ListaContato*)malloc(sizeof(ListaContato));

	

	novo->nome =(char*)malloc(strlen(nome));

	strcpy(novo->nome,nome);



	novo->telefone = (char*)malloc(strlen(telefone));
	strcpy(novo->telefone,telefone);


	novo->endereco = (char*)malloc(strlen(endereco));
	strcpy(novo->endereco,endereco);


	novo->prox = l;


	return novo;
}


void menu1(){

	printf("Digite:\n");
	printf("1 - Para adicionar um novo contato\n");
	printf("2 - Para retirar um contato\n");
	printf("3 - Para buscar um contato\n");
	printf("4 - Para imprimir todos os contatos\n");
	printf("5 - Para ler de um arquivo\n");
	printf("0 - Para sair do programa\n");
}


ListaContato** alocaAgenda(ListaContato **l,int n){


	int a;

	l = (ListaContato**)malloc(n*sizeof(ListaContato*));

	for(a = 0; a < n ;++a){
		l[a] = NULL;
	}

	return l;
}



int hash (char *key, int tam){

	int val_hash = 0, a = 127, k = 0;

	while(key[k] != '\0'){
		val_hash = val_hash * a + key[k];
		k++;
	}
	return abs(val_hash % tam);
}


