//#define N 50

/*struct pilha{

	int n;
	float vet[N];
};

typedef struct pilha Pilha;*/

struct lista{

	float info;
	struct lista *prox;
};

typedef struct lista Lista;

struct pilha{

	Lista *prim;
};

typedef struct pilha Pilha;

struct lista_int{
	int info;
	Pilha *cartas;
	struct lista_int *prox;
};

typedef struct lista_int Lista_int;


struct calc{

	char f[21];
	Pilha *p;
};

typedef struct calc Calc;

Pilha *pilha_cria();
void pilha_push(Pilha *p,float v);
float pilha_pop(Pilha *p);
int pilha_vazia(Pilha *p);
void pilha_libera(Pilha *p);
void pilha_imprime(Pilha *p);
Calc *calc_cria(char *formato);
void calc_operando(Calc *c, float v);
void calc_libera(Calc *c);
void calc_operador(Calc *c, char op);
Lista_int* lst_cria();

Lista_int* lst_insere(Lista_int *l, int i);

void lst_imprime_rec(Lista_int *l);

void lst_imprime(Lista_int *l);

int lst_vazia(Lista_int *l);

Lista_int* lst_busca(Lista_int *l, int v);

Lista_int *lst_retira(Lista_int *l, int v);

void lst_libera(Lista_int *l);
void lst_libera_rec(Lista_int *l);
Lista_int *lst_intercala(Lista_int *l1,Lista_int *l2);
Lista_int *lst_insere_ordenado(Lista_int *l, int v);

int lst_igual(Lista_int *l1, Lista_int *l2);

/*lista_ints circulares*/
void lcirc_imprime(Lista_int *l); 

/*listas duplamente encadeadas*/
/*Lista2_int *lst2_insere(Lista2_int *l, int v);

Lista2_int *lst2_busca(Lista2_int *l, int v);

Lista2_int *lst2_retira(Lista2_int *l, int v);*/