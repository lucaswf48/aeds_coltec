#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include "lwfunc.h"

#define DIM 17


typedef struct listaContato{

	char *nome;
	int codigo,idade;

	struct listaContato *prox;

}ListaContato;


ListaContato** opcao(ListaContato **l, int op);
ListaContato** lerArquivo(ListaContato **l,int n);
void buscaAgenda(ListaContato **l,int n);
void imprimiAgenda(ListaContato *l,int nome);
void imprimeListaAgenda(ListaContato **l,int n);
ListaContato* listaAgenda(ListaContato *l,char *nome, int code, int idad);
void menu1();
ListaContato** alocaAgenda(ListaContato **l,int n);
int hash (int key, int tam);

int main(){


	int escolha;

	ListaContato **agenda;

	limpaTela();

	agenda = alocaAgenda(agenda,DIM);

	

	do{

		menu1();
		scanf("%d",&escolha);
		limpaBuffer();
		agenda = opcao(agenda,escolha);


		continua();

	}while(escolha != 0);

}


ListaContato** opcao(ListaContato **l, int op){

	switch(op){
		case 1:
			l = lerArquivo(l,DIM);
			break;
		case 2:
			buscaAgenda(l,DIM);
			break;
		case 3:
			imprimeListaAgenda(l,DIM);
			break;
		case 0:
			break;
		default:
			printf("Opção invalida\n");
	}

	return l;
}

ListaContato** lerArquivo(ListaContato **l,int n){

	FILE *f;

	char nome[50];
	char *nome_inter;
	int code,idad;

	int i;


	srand(time(NULL));
	f = fopen("aviao.txt","r");



	do{

		fscanf(f,"%s %d",nome,&code);
			
		idad = rand() % 67 ; 



		nome_inter = nome;
		
		i = hash(code,17);


		l[i] = listaAgenda(l[i],nome_inter,code,idad);
		
		

	}while(!feof(f));

	return l;

}




void buscaAgenda(ListaContato **l,int n){

	
	int i = 0;
	int k;

	printf("Codigo do tripulante:");
	scanf("%d",&k);

	i = hash(k,17);

	

	imprimiAgenda(l[i],k);

}


void imprimiAgenda(ListaContato *l,int nome){

	ListaContato *p;

	for(p = l;p != NULL;p = p->prox){
		
	
			if(p->codigo == nome){
				
				printf("Nome: %s \n", p->nome);
				printf("Codigo: %d \n", p->codigo);
				printf("Idade: %d \n", p->idade);	
			}
	}

}

void imprimeListaAgenda(ListaContato **l,int n){

	int a;

	for(a=0;a<n;++a){
		ListaContato *p;
		for(p = l[a];p != NULL;p = p->prox){
			printf("Nome: %s \n", p->nome);
			printf("Codigo: %d \n", p->codigo);
			printf("Idade: %d \n\n", p->idade);	

			printf("\n");
		}
		
	}
}


ListaContato* listaAgenda(ListaContato *l,char *nome, int code, int idad){

	ListaContato *novo = (ListaContato*)malloc(sizeof(ListaContato));

	

	novo->nome =(char*)malloc(strlen(nome));
	strcpy(novo->nome,nome);

	novo->codigo = code;
	novo->idade = idad;

	novo->prox = l;

	return novo;
}


void menu1(){

	printf("Digite:\n");
	printf("1 - Ler arquivo\n");
	printf("2 - O tripulante K viaja neste vôo?\n");
	printf("3 - Para imprimir todos os contatos\n");

	printf("0 - Para sair do programa\n");
}


ListaContato** alocaAgenda(ListaContato **l,int n){


	int a;

	l = (ListaContato**)malloc(n*sizeof(ListaContato*));

	for(a = 0; a < n ;++a){
		l[a] = NULL;
	}

	return l;
}



int hash (int key, int tam){



	return (key % tam);
}


