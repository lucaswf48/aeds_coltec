#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "lwfunc.h"


//Complexidade o(n²)
void add(int a[3][3], int b[3][3]){

	int i,j;

	int resultado[3][3];	
	
	srand(time(NULL));

	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			a[j][i] = rand() % 11;
		}
	}

	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			b[j][i] = rand() % 11;
		}
	}

	printf("Matriz A:\n\n");
	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			printf("%d ",a[j][i]);
			
		}
		printf("\n");
	}

	printf("\n\n");

	printf("Matriz B:\n\n");
	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			printf("%d ",b[j][i]);
			
		}
		printf("\n");
	}


	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			resultado[j][i]  = a[j][i] + b[j][i];
			
		}
		
	}

	printf("\n\n");

	printf("Matriz A + B:\n\n");
	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			printf("%d ",resultado[j][i]);
			
		}
		printf("\n");
	}
	
}


//Complexidade o(n³)
void multiply(int a[3][3], int b[3][3]){

	int i,j,v;
	int resultado[3][3];
	
	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			a[j][i] = rand() % 11;
		}
	}

	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			b[j][i] = rand() % 11;
		}
	}


	printf("Matriz A:\n\n");
	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			printf("%d ",a[j][i]);
			
		}
		printf("\n");
	}

	printf("\n\n");

	printf("Matriz B:\n\n");
	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			printf("%d ",b[j][i]);
			resultado[i][j] = 0;
		}
		printf("\n");
	}

	for(i = 0; i < 3;i++){
		for(j = 0; j < 3;j++){
			for(v = 0; v < 3;v++){
				resultado[i][j] =  resultado[i][j] + (a[i][v] * b[v][j]);
			}
		}
	}


	printf("\n\n");

	printf("Matriz A * B:\n\n");
	for(j = 0;j < 3;++j){
		for(i = 0;i < 3;++i){
			
			printf("%d ",resultado[j][i]);
			
		}
		printf("\n");
	}
}

int main(){

	void (*pfun[2])(int a[3][3],int b[3][3]) = {add,multiply};

	int escolha;

	int a[3][3],b[3][3];

	limpaTela();

	do{
		printf("Escolha:\n");
		printf("0 - Para somar\n");
		printf("1 - Para multiplicar\n");
		printf("2 - Para sair\n");
		scanf("%d",&escolha);
		limpaBuffer();
		switch(escolha){
			case 0:
				pfun[escolha](a,b);
				break;
			case 1:
				pfun[escolha](a,b);
				break;
			case 2:
				break;
			default:
				printf("opção invalida\n");


		}

			continua();

	}while(escolha != 2);
}