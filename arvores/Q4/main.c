#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "lwfunc.h"

Arv* insereFraseNaArvore(Arv *a,char *frase);
void istogramaArvore(Arv *arvore,int *v);

int main(){

	char *frase;
	int *vet = alocaVetorInt(26),
		 cont;

	Arv *arvore = arvoreInicializa();

	printf("Digite uma frase:\n");
	frase = lerStringN();

	arvore = insereFraseNaArvore(arvore,frase);

	istogramaArvore(arvore,vet);

	for(cont = 0;cont < 26;++cont){
		printf("%c = %d\n",'a'+cont,vet[cont]);
	}
	free(vet);
	return 0;
}

Arv* insereFraseNaArvore(Arv *a,char *frase){

	int cont;
	for(cont = 0;cont < strlen(frase); ++cont){

		a = arvoreInsere(a,frase[cont]);
	}

	return a;
}

void istogramaArvore(Arv *arvore,int *v){


	if(!(arvoreVazia(arvore))){
		istogramaArvore(arvore->esq,v); /* mostra sae */
		istogramaArvore(arvore->dir,v); /* mostra sad */
		v[arvore->info - 97]++;
	}
}
