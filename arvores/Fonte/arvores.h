
struct arv {
 char info;
 struct arv* esq;
 struct arv* dir;
};

typedef struct arv Arv;

struct arvvar{
	char info;
	struct arvvar *prim;
	struct arvvar *prox;
};

typedef struct arvvar ArvVar;

Arv* inicializa(void);
Arv* cria(char c, Arv* sae, Arv* sad);
int vazia(Arv* a);
void imprime_pre (Arv* a);
void imprime_simetrica(Arv* a);
void imprime_pos(Arv* a);
Arv* insere(Arv *a,char carac);
Arv* libera (Arv* a);
int busca (Arv* a, char c);
int max2 (int a ,int b);
int arv_altura(Arv *a);
ArvVar *arvv_cria(char c);
void arvv_insere(ArvVar *a,ArvVar *sa);
void arvv_imprime(ArvVar *a);
int arvv_pertence(ArvVar *a, char c);
void arvv_libera(ArvVar *a);
int arvv_altura(ArvVar *a);