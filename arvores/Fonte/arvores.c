#include <stdio.h>
#include <stdlib.h>
#include "arvores.h"

Arv* inicializa(void){

	return NULL;
}

Arv* cria(char c, Arv* sae, Arv* sad){

	Arv* p=(Arv*)malloc(sizeof(Arv));
	p->info = c;
	p->esq = sae;
	p->dir = sad;
	return p;
}

int vazia(Arv* a){
	
	return a==NULL;
}

void imprime_pre (Arv* a){

	if (!vazia(a)){
	printf("%c ", a->info); /* mostra raiz */
	imprime_pre(a->esq); /* mostra sae */
	imprime_pre(a->dir); /* mostra sad */
	}
}

void imprime_simetrica(Arv* a){

	if(!vazia(a)){


	imprime_simetrica(a->esq); /* mostra sae */
	printf("%c ", a->info); /* mostra raiz */
	imprime_simetrica(a->dir); /* mostra sad */	
	}
}

void imprime_pos(Arv* a){

	if(!vazia(a)){


	imprime_pos(a->esq); /* mostra sae */
	
	imprime_pos(a->dir); /* mostra sad */	
	printf("%c ", a->info); /* mostra raiz */
	}
}

Arv* libera (Arv* a){

	if (!vazia(a)){

		libera(a->esq); /* libera sae */
		libera(a->dir); /* libera sad */
		free(a); /* libera raiz */
	}
	return NULL;
}

int busca (Arv* a, char c){
	if (vazia(a))
		return 0; /* árvore vazia: não encontrou */
	else
		return a->info==c || busca(a->esq,c) || busca(a->dir,c);
}

int max2(int a, int b){

	return (a > b) ? a : b;
}

int arv_altura(Arv *a){

	if(vazia(a))
		return -1;
	else
		return 1 + max2(arv_altura(a->esq), arv_altura(a->dir));
}

Arv* insere(Arv *a,char carac){
	
	if(a == NULL){

		a = (Arv*) malloc(sizeof(Arv));
		a->info = carac;
		a->esq = a->dir = NULL;
	}
	else if(carac < a->info)
		a->esq = insere(a->esq,carac);
	else 
		a->dir = insere(a->dir,carac);
	return a;
}

ArvVar *arvv_cria(char c){

	ArvVar *a = (ArvVar*) malloc(sizeof(ArvVar));
	a->info = c;
	a->prim = NULL;
	a->prox = NULL;
	return a;

}

void arvv_insere(ArvVar *a,ArvVar *sa){

	sa->prox = a->prim;
	a->prim = sa;
}

void arvv_imprime(ArvVar *a){

	ArvVar *p;
	printf("%c \n", a->info);
	for(p = a->prim; p!= NULL; p = p->prox)
		arvv_imprime(p);
}


int arvv_pertence(ArvVar *a, char c){
	ArvVar *p;
	if(a->info == c){
		return 1;
	}
	else{
		for(p = a->prim; p!= NULL;p= p->prox){
			if(arvv_pertence(p,c))
				return 1;
		}
		return 0;
	}
}

void arvv_libera(ArvVar *a){

	ArvVar *p = a->prim;

	while(p != NULL){

		ArvVar *t = p->prox;
		arvv_libera(p);
		p = t; 
	}
	free(a);
}

int arvv_altura(ArvVar *a){

	int hmax= -1;
	ArvVar *p;

	for(p = a->prim;p!=NULL;p = p->prox){

		int h = arvv_altura(p);
		if(h > hmax)
			hmax = h;
	}

	return hmax + 1;
}