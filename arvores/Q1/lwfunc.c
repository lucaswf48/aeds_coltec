#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "lwfunc.h"

#define DIM 100

//Se o sistema operacional for linux
#ifdef linux

//Cria a função para limpar o buffer com o __fpurge(stdin)
void limpaBuffer(){

	__fpurge(stdin);
}

//Cria a função que limpa a tela com system("clear")
void limpaTela(){

	system("clear");
}



//Se o sistema operacional for windows
#elif WIN32


//Cria a função para limpar o buffer com o fflush(stdin)
void limpaBuffer(){

	fflush(stdin);
}

//Cria a função que limpa a tela com system("cls")
void limpaTela(){

	system("cls");
}

#endif

void continua(){

    printf("\nAperte \"ENTER\" para continuar");
    limpaBuffer();
    getchar();
    limpaTela();
}
//
char* lerStringN(){

	char *frase = (char*)malloc(sizeof(char));
	int cont = 0;

	do{
		
		frase[cont] = getchar();
		if(frase[cont] == '\n'){
			frase[cont] = '\0';
			break;
		}
		else{
			++cont;
			frase = (char*)realloc(frase,(cont)*sizeof(char));
		}
	}while(1);
	
	return frase;
}
//
void lerVetorInt(int vet[],int tam){

	int j;
	for(j = 0;j < tam;++j){
		printf("Posição %d:\n",j);
		scanf("%d",&vet[j]);
	}
	printf("\n");
}

void imprimeVetorInt(int vet[],int tam){

	int j;
	for(j = 0;j < tam;++j){
		printf("%d, ",vet[j]);
	}
	printf("\n");
}

//Recebe como parametro o tamanho N do vetor
int *alocaVetorInt(int n){

	int *ponteiro;

	ponteiro = (int*)calloc(n,sizeof(int));

	return ponteiro;
}

int *liberaVetorInt(int *p){

	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Primeiro parametro linha segundo coluna
void lerMatrizInt(int **p,int a, int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;++i){
			printf("Posição [%d] [%d]\n", j,i);
			scanf("%d",&p[j][i]);
		}
	}
}

void imprimeMatrizInt(int **p,int a, int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;++i){
			printf("%d ",p[j][i]);
		}
		printf("\n");
	}
}

int** alocaMatrizInt(int a,int b){

	int **ponteiro,i;

	ponteiro = (int**)calloc(a,sizeof(int*));

	for(i = 0;i < a;++i){

		ponteiro[i] =(int*)calloc(b,sizeof(int));
	}

	return ponteiro;
}

int **liberaMatrizInt(int **p,int a){

	int i;

	for(i = 0;i<a;i++){
		free(p[i]);
	}
	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerVetorFloat(float vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		scanf("%f",&vet[j]);
	}
	printf("\n");
}

void imprimeVetorFloat(float vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		printf("%f, ",vet[j]);
	}
	printf("\n");
}

//Recebe como parametro o tamanho N do vetor
float* alocaVetorFloat(int n){

	float *ponteiro;

	ponteiro =(float*)calloc(n,sizeof(float));

	return ponteiro;
}

float* liberaVetorFloat(float *p){

	free(p);

	return p;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerMatrizFloat(float **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			scanf("%f",&p[j][i]);
		}
	}
}

void imprimeMatrizFloat(float **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			printf("%f",p[j][i]);
		}
		printf("\n");
	}
}

float** alocaMatrizFloat(int a,int b){

	float **ponteiro;
	int i;

	ponteiro = (float**)calloc(a,sizeof(float*));

	for(i = 0; i < a;++i){
		ponteiro[i] = (float*)calloc(b,sizeof(float));
	}

	return ponteiro;
}

float** liberaMatrizFloat(float **p, int a){

	int i;

	for(i = 0;i<a;i++){
		free(p[i]);
	}
	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerVetorDouble(double vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		scanf("%lf",&vet[j]);
	}
	printf("\n");
}

void imprimeVetorDouble(double vet[],int tam){

	int j;

	for(j = 0;j < tam;++j){
		printf("%lf, ",vet[j]);
	}
	printf("\n");
}

//Recebe como parametro o tamanho N do vetor
double* alocaVetorDouble(int n){

	double *ponteiro;

	ponteiro =(double*)calloc(n,sizeof(double));

	return ponteiro;
}

double* liberaVetorDouble(double *p){

	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void lerMatrizDouble(double **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			scanf("%lf",&p[j][i]);
		}
	}
}

void imprimeMatrizDouble(double **p,int a,int b){

	int i,j;

	for(j = 0;j < a;++j){
		for(i = 0;i < b;i++){
			printf("%lf",p[j][i]);
		}
		printf("\n");
	}
}

double** alocaMatrizDouble(int a,int b){

	double **ponteiro;
	int i;

	ponteiro = (double**)calloc(a,sizeof(double*));

	for(i = 0; i < a;++i){
		ponteiro[i] = (double*)calloc(b,sizeof(double));
	}

	return ponteiro;
}

double** liberaMatrizDouble(double **p,int a){


	int i;

	for(i = 0;i < a;++i){
		free(p[i]);
	}


	free(p);

	return p;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void bubbleSort(int vet[],int t){
	int b,a,al;
	for (a = 0; a <= t;a++){
		 for(b = 0; b <= t;b++){
			 if (vet[a] < vet[b]){
			 al = vet[a];
			 vet[a] = vet[b];
			 vet[b] = al;
			 }
		 }
	 }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

int partition(int a[],int l, int r){
	int pivot,i,j,aux;
	pivot = a[l];
	i=l;
	j=r+1;
	while(1){
		do ++i;while(a[i] <= pivot && i <= r);
		do --j;while(a[j] > pivot);
		if(i >= j)break;
		aux = a[i];
		a[i]=a[j];
		a[j]= aux;
	}
	aux= a[l];
	a[l]= a[j];
	a[j]= aux;
	return j;
}

void quickSort(int a[],int l,int r){
	int j;
	if(l < r){
		j=partition(a,l,r);
		quickSort(a,l,j-1);
		quickSort(a,j+1,r);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void merge(int arr[], int l, int m, int r){

    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;


    int L[n1], R[n2];


    for(i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for(j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];


    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2){
        if (L[i] <= R[j]){

            arr[k] = L[i];
            i++;
        }
        else{

            arr[k] = R[j];
            j++;
        }
        k++;
    }


    while (i < n1){
        arr[k] = L[i];
        i++;
        k++;
    }


    while (j < n2){
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int arr[], int l, int r){
    if (l < r){

        int m = l+(r-l)/2;
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);
        merge(arr, l, m, r);
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Função que retorna a hora atual no formato hora/minuto/segundo, na forma de uma string
char* escreveHoraFormatada(){

	int segundo,
		minuto,
		hora;

	char segundoS[DIM],
		minutoS[DIM],
		horaS[DIM],
		*horaformatado;

	horaformatado = (char *)malloc(DIM*sizeof(char));

	time_t hora_atual = time (NULL);
	struct tm *t = localtime (&hora_atual);

	segundo = t->tm_sec;
	minuto = t->tm_min;
	hora = t->tm_hour;

	sprintf(horaS,"%02d",hora);
	sprintf(minutoS,"%02d",minuto);
	sprintf(segundoS,"%02d",segundo);
	sprintf(horaformatado,"%s:%s:%s",horaS,minutoS,segundoS);

	return horaformatado;
}

//Função que retorna a hora atual no formato dd/mm/aaaa, na forma de uma string
char* escreveDataFormatada(){


	int dia,
		mes,
		ano;

	char diaS[DIM],
		mesS[DIM],
		anoS[DIM],
		*dataformatado;

	dataformatado = (char *)malloc(DIM*sizeof(char));

	time_t hora_atual = time (NULL);
	struct tm *t = localtime (&hora_atual);

	dia = t->tm_mday;
	mes = t->tm_mon + 1;
	ano = t->tm_year + 1900;

	sprintf(diaS,"%02d",dia);
	sprintf(mesS,"%02d",mes);
	sprintf(anoS,"%02d",ano);
	sprintf(dataformatado,"%s/%s/%s",diaS,mesS,anoS);

	return dataformatado;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Lista* listaCria(){

	return NULL;
}

Lista* listaInsere(Lista *l, int i){

	Lista *novo = (Lista *)malloc(sizeof(Lista));

	novo->info = i;
	novo->prox = l;

	return novo;
}

void listaImprimeRec(Lista *l){

	if(l == NULL){
		return;
	}
	else{
		printf("Info %d \n",l->info);
		listaImprimeRec(l->prox);
	}


}


void listaImprime(Lista *l){

	Lista *p;

	for(p = l;p != NULL;p = p->prox){
		printf("Info %d \n", p->info);
		//printf("prox %p \n", p->prox);
	}
}

int listaVazia(Lista *l){

	return (l == NULL);
}

Lista* listaBusca(Lista *l, int v){

	Lista *p;

	for(p = l; p != NULL; p = p->prox){
		if(p->info == v){
			return p;
		}
	}

	return NULL;
}

Lista *listaRetira(Lista *l, int v){

	Lista *ant = NULL;
	Lista *p = l;

	while(p != NULL && p->info != v){

		ant = p;
		p = p->prox;
	}

	if(p == NULL)
		return l;

	if(ant == NULL)
		l = p->prox;
	else
		ant->prox = p->prox;

	free(p);

	return l;
}

Lista *listaRetiraRec(Lista *l, int v){

	if(l == NULL){
		return 0;
	}
	else{
		if(l->info==v){
			l=l->prox;
		}
		else{
				l->prox = listaRetiraRec(l->prox,v);
		}
	}
	return l;
}

void listaLibera(Lista *l){

	Lista *p = l;
	while(p != NULL){

		Lista *t = p->prox;
		free(p);
		p = t;
	}
}

void listaLiberaRec(Lista *l){

	if(l == NULL){
		return;
	}
	else{

		listaLiberaRec(l->prox);
		free(l);
	}

}

Lista *listaInsereOrdenado(Lista *l, int v){

	Lista *novo;
	Lista *ant = NULL;
	Lista *p = l;

	while(p != NULL && p->info < v){
		ant = p;
		p = p->prox;
	}

	novo = (Lista*)malloc(sizeof(Lista));
	novo->info = v;

	if(ant == NULL){
		novo->prox = l;
		l = novo;
	}
	else{
		novo->prox = ant->prox;
		ant->prox = novo;
	}
	return l;
}

int listaIgual(Lista *l1, Lista *l2){

	Lista *p = l1, *p1=l2;


	while(1){

		if(p->info!=p1->info){

			return 0;
		}
		if(p1->prox == NULL && p->prox==NULL){
			break;
		}
		p = p->prox;
		p1 = p1->prox;
	}

	return 1;
}

Lista *listaIntercala(Lista *l1,Lista *l2){

	Lista *p1 = l1,*p2 = l2,*d1,*d2;
	int i=0;

	while(p1 != NULL || p2 != NULL){


		if(p1->prox == NULL && p2->prox != NULL){
			if(i==0){
			p1->prox=l2;
			break;
			}
			else{
				p1->prox=d2;
				break;
			}
		}
		if(p1->prox != NULL && p2->prox == NULL){

			if(i==0){
				d1=p1->prox;
				p1->prox=p2;
				p2->prox=d1;
				break;
			}
			else{
				d1=p1->prox;
				p1->prox=d2;
				p1->prox->prox=d1;
				break;
			}
		}
		d1=p1->prox;
		d2=p2->prox;
		p1->prox=p2;
		p1->prox->prox=d1;
		p2=d2;
		p1=d1;
		i++;

	}



	return l1;
}



/*listas circulares*/
void lcircImprime(Lista *l){

	Lista *p = l;
	if(p)
		do{
			printf("%d\n", p->info);
			p=p->prox;
		}while(p!=l);
}

void listaCircularLibera(Lista *l){

	Lista *p = l;
	if(p)
		do{
			Lista *t = p->prox;
			free(p);
			p = t;
			p=p->prox;
		}while(p!=l);

}

/*listas duplamente encadeadas*/
Lista2 *lista2InsereDupla(Lista2 *l, int v){

	Lista2 *novo = (Lista2 *)malloc(sizeof(Lista2));
	Lista2 *daw;
	novo->info = v;
	novo->prox = l;
	daw=l->ant;
	daw->prox=novo;
	novo->ant = daw;
	if(l != NULL){
		l->ant = novo;
	}

	return novo;
}

Lista2 *lista2Insere(Lista2 *l, int v){

	Lista2 *novo = (Lista2 *)malloc(sizeof(Lista2));
	novo->info = v;
	novo->prox = l;
	novo->ant = NULL;
	if(l != NULL){
		l->ant = novo;
	}

	return novo;
}




Lista2 *lista2Busca(Lista2 *l, int v){

	Lista2 *p;

	for(p = l;p!= NULL;p=p->prox){
			if(p->info == v){
				return p;
			}
	}

	return NULL;
}

Lista2 *lista2Retira(Lista2 *l, int v){

	Lista2 *p = lista2Busca(l,v);
	if(p == NULL){
		return l;
	}

	if(l==p)
		l = p->prox;
	else
		p->ant->prox = p->prox;

	if(p->prox != NULL)
		p->prox->ant = p->ant;

	free(p);

	return l;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Fila *filaCria(){
	Fila *f = (Fila *)malloc(sizeof(Fila));
	f->ini = f->fim = NULL;
	return f;
}

void filaInsere(Fila *f,float v){

	Lista *n = (Lista *)malloc(sizeof(Lista));
	n->info = v;
	n->prox = NULL;
	if(f->fim != NULL)
		f->fim->prox = n;
	else
		f->ini = n;
	f->fim = n;

}

float filaRetira(Fila *f){

	Lista *t;
	float v;
	if(filaVazia(f)){
		printf("Vazia\n");
		exit(1);
	}
	t = f->ini;
	v = t->info;
	f->ini = t->prox;
	if(f->ini ==NULL)
		f->fim = NULL;
	free(t);
	return v;
}

int filaVazia(Fila *f){

	return (f->ini == NULL);
}

void filaLibera(Fila *f){

	Lista *q = f->ini;
	while(q != NULL){

		Lista *t = q->prox;
		free(q);
		q = t;
	}
	free(f);
}
void filaImprime(Fila *f){
	Lista *q;
	for(q = f->ini; q!= NULL; q = q->prox)
		printf("%d \n", q->info);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Pilha *pilhaCria(){

	Pilha *p = (Pilha*)malloc(sizeof(Pilha));
	p->prim = NULL;
	return p;
}

void pilhaPush(Pilha *p,float v){

	Lista *n = (Lista *)malloc(sizeof(Lista));
	n->info = v;
	n->prox = p->prim;
	p->prim = n;
}

float pilhaPop(Pilha *p){

	Lista *t;
	float v;
	if(pilhaVazia(p)){

		return -1;
	}
	t = p->prim;
	v = t->info;
	p->prim = t->prox;
	free(t);
	return v;
}

int pilhaVazia(Pilha *p){
	return (p->prim == NULL);
}

void pilhaLibera(Pilha *p){

	Lista *q = p->prim;
	while(q != NULL){
		Lista *t = q->prox;
		free(q);
		q = t;
	}

	free(q);
}


void pilhaImprime(Pilha *p){

	Lista *q;
	for(q = p->prim;q != NULL;q = q->prox)
		printf("%d ",q->info);
	printf("\n");
	printf("\n");
}

/////////////////////////////////////////////////////////////////////////////////////

Calc *calcCria(char *formato){

	Calc *c = (Calc*)malloc(sizeof(Calc));
	strcpy(c->f, formato);
	c->p = pilhaCria(); //cria pilha vazia
	return c;
}

void calcOperando(Calc *c, float v){

	pilhaPush(c->p, v); //empilha operando
	printf(c->f, v); //imprime topo
}

void calcLibera(Calc *c){

	pilhaLibera(c->p);
	free(c);
}

void calcOperador(Calc *c, char op){
	float v1, v2, v;
	//desempilha operandos
	if (pilhaVazia(c->p))
		v2 = 0.0;
	else
		v2 = pilhaPop(c->p);
	if (pilhaVazia(c->p))
		v1 = 0.0;
	else
		v1 = pilhaPop(c->p);
	//faz operacao
	switch(op){

		case '+':
			v = v1 + v2;
			break;
		case '-':
			v = v1 - v2;
			break;
		case '*':
			v = v1 * v2;
			break;
		case '/':
			v = v1 / v2;
			break;
	}
	//empilha resultado
	pilhaPush(c->p, v);
	//imprime topo da pilha
	printf(c->f, v);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Arv* arvoreInicializa(void){

	return NULL;
}

Arv* arvoreCria(char c, Arv* sae, Arv* sad){

	Arv* p=(Arv*)malloc(sizeof(Arv));
	p->info = c;
	p->esq = sae;
	p->dir = sad;
	return p;
}

int arvoreVazia(Arv* a){

	return a==NULL;
}

void arvoreImprimePre (Arv* a){

	if (!arvoreVazia(a)){
	printf("%c ", a->info); /* mostra raiz */
	arvoreImprimePre(a->esq); /* mostra sae */
	arvoreImprimePre(a->dir); /* mostra sad */
	}
}

void arvoreImprimeSim(Arv* a){

	if(!arvoreVazia(a)){


	arvoreImprimeSim(a->esq); /* mostra sae */
	printf("%c ", a->info); /* mostra raiz */
	arvoreImprimeSim(a->dir); /* mostra sad */
	}
}

void arvoreImprimePos(Arv* a){

	if(!arvoreVazia(a)){


	arvoreImprimePos(a->esq); /* mostra sae */

	arvoreImprimePos(a->dir); /* mostra sad */
	printf("%c ", a->info); /* mostra raiz */
	}
}

Arv* arvoreLibera (Arv* a){

	if (!arvoreVazia(a)){

		arvoreLibera(a->esq); /* libera sae */
		arvoreLibera(a->dir); /* libera sad */
		free(a); /* libera raiz */
	}
	return NULL;
}

int arvoreBusca(Arv* a, char c){
	if (arvoreVazia(a))
		return 0; /* árvore vazia: não encontrou */
	else{
		//printf("%d",a->info==c || busca(a->esq,c) || busca(a->dir,c));
		return a->info==c || arvoreBusca(a->esq,c) || arvoreBusca(a->dir,c);
	}
}

int arvoreMax2(int a, int b){

	return (a > b) ? a : b;
}

int arvoreAltura(Arv *a){

	if(arvoreVazia(a))
		return -1;
	else
		return 1 + arvoreMax2(arvoreAltura(a->esq), arvoreAltura(a->dir));
}


Arv* arvoreInsere(Arv *a,char carac){

	if(a == NULL){

		a = (Arv*) malloc(sizeof(Arv));
		a->info = carac;
		a->esq = a->dir = NULL;
	}
	else if(carac < a->info)
		a->esq = arvoreInsere(a->esq,carac);
	else
		a->dir = arvoreInsere(a->dir,carac);
	return a;
}


void arvoreContaFolhasFilhos(Arv *p,int *folhas,int *filho1,int *filho2){

    if(!arvoreVazia(p)){
        if(p->esq == NULL && p->dir==NULL){

            ++(*folhas);

        }
        else if((p->esq == NULL && p->dir!=NULL) || (p->esq != NULL && p->dir==NULL)){
            ++(*filho1);
        }
        else if(p->esq != NULL && p->dir != NULL){
            ++(*filho2);
        }
        arvoreContaFolhasFilhos(p->esq,folhas,filho1,filho2);
        arvoreContaFolhasFilhos(p->dir,folhas,filho1,filho2);
    }

}


/*void arvoreContaCaracter(Arv *a, int clear) {

       if (a == NULL)
            return;

        if (clear)
            memset(vet, 0, sizeof vet);

        arv_alfa_count(a->esq, 0);
        arv_alfa_count(a->dir, 0);
        vet[a->info - 'a']++;
}*/

////////////////////////////////////////////////////////////////////////////////////////////////

ArvVar *arvv_cria(char c){

	ArvVar *a = (ArvVar*) malloc(sizeof(ArvVar));
	a->info = c;
	a->prim = NULL;
	a->prox = NULL;
	return a;

}

void arvv_insere(ArvVar *a,ArvVar *sa){

	sa->prox = a->prim;
	a->prim = sa;
}

void arvv_imprime(ArvVar *a){

	ArvVar *p;
	printf("%c \n", a->info);
	for(p = a->prim; p!= NULL; p = p->prox)
		arvv_imprime(p);
}


int arvv_pertence(ArvVar *a, char c){
	ArvVar *p;
	if(a->info == c){
		return 1;
	}
	else{
		for(p = a->prim; p!= NULL;p= p->prox){
			if(arvv_pertence(p,c))
				return 1;
		}
		return 0;
	}
}

void arvv_libera(ArvVar *a){

	ArvVar *p = a->prim;

	while(p != NULL){

		ArvVar *t = p->prox;
		arvv_libera(p);
		p = t;
	}
	free(a);
}

int arvv_altura(ArvVar *a){

	int hmax= -1;
	ArvVar *p;

	for(p = a->prim;p!=NULL;p = p->prox){

		int h = arvv_altura(p);
		if(h > hmax)
			hmax = h;
	}

	return hmax + 1;
}

////////////////////////////////////////////////////////////////////////////////////////////////

void arvoreAvlRotacionaDireita(pno p){

	pno q, temp;
	q = p->esq;
	temp = q->dir;
	q->dir = p;
	p->esq = temp;
	p = q;
}

void arvoreAvlRotacionaEsquerda(pno p){

	pno q,temp;
	q = p->dir;
	temp = q->esq;
	q->esq = p;
	p->dir = temp;
	p = q;
}

void arvoreAvlRotacionaEsquerdaDireita(pno p){

	pno u,v;
	u = p->esq;
	v = u->dir;
	u->dir = v->esq;
	v->esq = u;
	p->esq = v;
	v->dir = p;
	if(v->bal == -1){
		u->bal = 0;
		p->bal = 1;
	}
	else{

		p->bal = 0;
		u->bal = -1;
	}
	p = v;
}

void arvoreAvlRotacionaDireitaEsquerda(pno p){

	pno z,v;
	z = p->dir;
	v = z ->esq;
	z->esq = v->dir;
	v->dir = z;
	p->dir = v->esq;
	v->esq = p;
	if(v->bal == 1){
		p->bal = -1;
		z->bal = 0;
	}
	else{

		p->bal = 0;
		z->bal = 1;
	}

	p = v;
}

void arvoreAvlInsere(int x, pno p,bool kank){

	if(p == NULL){

		arvoreAvlAloca(p,x);
		kank = true;
		return;
	}
	if(x < p->info){
		arvoreAvlInsere(x,p->esq,kank);
		if(kank)
			switch(p->bal){
				case 1:
						p->bal = 0;
						kank = false;
						break;
				case 0:
						p->bal = -1;
						break;
				case -1:
						arvoreAvlCaso1(p);
						kank = false;
						break;
			}
		return;
	}

	if(x > p->info){

		arvoreAvlInsere(x,p->dir,kank);
		if(kank)
			switch(p->bal){
				case -1:
						p->bal = 0;
						kank = false;
						break;
				case 1:
						arvoreAvlCaso2(p);
						kank = false;
						break;
			}
		return;
	}
}

void arvoreAvlAloca(pno p, int x){

	p = malloc(sizeof(no));
	p->esq = NULL;
	p->dir = NULL;
	p->info = x;
	p->bal = 0;
}

void arvoreAvlCaso1(pno p){

	pno u;
	u = p->esq;
	if(u->bal == -1)
		arvoreAvlRotacionaDireita(p);
	else
		arvoreAvlRotacionaEsquerdaDireita(p);

	p->bal = 0;
}

void arvoreAvlCaso2(pno p){

	pno u;
	u = p->dir;
	if(u->bal == 1)
		arvoreAvlRotacionaEsquerda(p);
	else
		arvoreAvlRotacionaDireitaEsquerda(p);
	p->bal = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

Grafo* alocaGrafo(){

	return (Grafo*) malloc(sizeof(Grafo));
}

void alocaMatrizAdjacente(Grafo *g, int n,bool ori,bool pon){

	int cont;

	g->matriz = (int**)calloc(n,sizeof(int*));

	for(cont = 0;cont < 6;++cont)
		g->matriz[cont] = (int*)calloc(n,sizeof(int));

	g->numero_vertices = n;
	g->orientado = ori;
	g->ponderado = pon;		
}

void imprimeMatrizAdjacente(Grafo *g){

	int j,i;

	for(i = 0;i < g->numero_vertices;++i){
		printf("%d: ",i+1);
		for(j = 0;j < g->numero_vertices;++j){

			printf("%2d ",g->matriz[i][j]);
		}
		printf("\n");
	}
}

void insereMarizAdjacente(Grafo *g, int vertice1, int vertice2){


	int peso;

	if(g->orientado){

		if(g->ponderado){
			printf("Qual é o peso de %d para %d:",vertice1,vertice2);
			scanf("%d",&peso);
			g->matriz[vertice1-1][vertice2-1] = peso;
			g->matriz[vertice2-1][vertice1-1] = -1;
		}
		else{
			g->matriz[vertice1-1][vertice2-1] = 1;
			g->matriz[vertice2-1][vertice1-1] = -1;
		}
	}
	else{
		if(g->ponderado){
			printf("Qual é o peso de %d para %d:",vertice1,vertice2);
			scanf("%d",&peso);
			g->matriz[vertice1-1][vertice2-1] = peso;
			g->matriz[vertice2-1][vertice1-1] = peso;
		}
		else{
			g->matriz[vertice1-1][vertice2-1] = 1;
			g->matriz[vertice2-1][vertice1-1] = 1;
		}
	}
}

bool avaliaVertices(Grafo *g,int a,int b){

	if((a >=0 && a <= g->numero_vertices) && (b >=0 && b <= g->numero_vertices))
		return true;
	else
		return false;
}

void imprimeCaracteristicas(Grafo *g){


	printf("Ponderado: %d\n",g->ponderado);
	printf("Orientado: %d\n",g->orientado);
	printf("Numero de vertices: %d\n",g->numero_vertices);

	printf("\n");
}

void grauVertice(Grafo *g){

	int j,i,conta_grau = 0;

	for(j = 0;j < g->numero_vertices;++j){
		for(i = 0;i < g->numero_vertices;++i){
			if(g->matriz[j][i] != 0 && g->matriz[j][i] != -1)
				++conta_grau;
		}
		printf("Grau do vertice %d: %d\n",j+1,conta_grau);
		conta_grau = 0;
	}
}

////////////////////////////////////////////////////////////////////////////

GrafoLista* alocaGrafoLista(){

	return (GrafoLista*)malloc(sizeof(GrafoLista));
}

void criaListaAdjacencias(GrafoLista *gl, int n,bool pon){

	int i;

	gl->vet_lista = (Lista**)malloc(n*sizeof(Lista));
	
	gl->numero_vertices = n;
	gl->ponderado = pon;

	for(i = 0; i < n;++i){
		gl->vet_lista[i] = listaCria();
	} 
}

void insereListaAdjacencias(GrafoLista *gl,int a,int b){

	gl->vet_lista[a-1] = listaInsere(gl->vet_lista[a-1],b-1); 
}

void imprimeListaAdjacencias(GrafoLista *gl){

	int a;

	for(a=0;a<gl->numero_vertices;++a){
		printf("Vertices adjacentes a %d\n",a+1);
		Lista *p;
		for(p = gl->vet_lista[a];p != NULL;p = p->prox){
			printf("Info %d \n", p->info + 1);	
		}
	}
}
