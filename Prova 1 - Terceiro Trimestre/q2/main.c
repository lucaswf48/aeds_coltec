#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "lwfunc.h"

void menu(void);
Arv* insereCaracter(Arv *a);
void imprimeTeste(Arv *a);
Arv* arvoreRetira1(Arv *a);

Arv* arvoreRetira2(Arv *a,int car);
Arv *direitaDaEsquerda(Arv *a);

int main(void){

	Arv *teste = arvoreInicializa();
	int escolha;

	do{
		menu();
		scanf("%d",&escolha);
		limpaBuffer();
		switch(escolha){
			case 1:
				teste = insereCaracter(teste);				
				break;
			case 2:
				teste = arvoreRetira1(teste);
				break;
			case 3:
				imprimeTeste(teste);
				break;
			case 0:
				break;
		}
		continua();
	}while(escolha != 0);

	arvoreLibera(teste);
}

void menu(void){

	printf("Digite:\n");
	printf("1 - Para adicionar caracter na arvore\n");
	printf("2 - Para retirar caracter na arvore\n");
	printf("3 - Para imprimir arvore\n");
	printf("0 - Para sair do programa\n");
}

Arv* insereCaracter(Arv *a){

	int car;
	printf("Digite uma letra minuscula:\n");
	scanf("%d",&car);
	a = arvoreInsere(a,car);

	return a;
}

void imprimeTeste(Arv *a){

	if(arvoreVazia(a)){
		printf("Arvore Vazia!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	}
	else{

		arvoreImprimePre(a);
	}
}

Arv* arvoreRetira1(Arv *a){

	if(arvoreVazia(a)){
		printf("Arvore Vazia!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		return NULL;
	}
	else{
		
		int caracter;
		printf("Digite o caracter a ser retirado da arvore:");
		scanf("%d",&caracter);
		if(arvoreBusca(a,caracter)){
			a = arvoreRetira2(a,caracter);
			return a;
		}
		else{
			printf("Caracter não encontrado\n");
		}
	}
}

Arv* arvoreRetira2(Arv *a,int car){


	Arv *kank,*daw;
	if(!arvoreVazia(a)){

		if(a->info == car){
			if(a->dir == NULL && a->esq == NULL){
				
				free(a);
				return NULL;
			}
			else if(a->dir == NULL && a->esq != NULL){
				
				kank = a->esq;
				free(a);
				return kank;
			}
			else if(a->dir != NULL && a->esq == NULL){
				
				
				kank = a->dir;
				free(a);
				return kank;
			}
			else if(a->dir != NULL && a->esq != NULL){
				//não funciona
				kank = direitaDaEsquerda(a->esq);
				kank->dir = a->dir;
				free(a);
				return kank;
			}
		}

		a->esq = arvoreRetira2(a->esq,car);
		a->dir = arvoreRetira2(a->dir,car);
	}

	return a;
}	
//não funciona
Arv *direitaDaEsquerda(Arv *a){

	if(arvoreVazia(a)){
		direitaDaEsquerda(a->dir);
		return a;
	}

}