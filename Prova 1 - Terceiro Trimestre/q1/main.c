#include <stdio.h>
#include <stdbool.h>
#include "lwfunc.h"

void menu(void);
void adicionaProcesso(Fila *f);
int verificaProcesso(int identificadorTesta,Fila *f);
void imprimeProcessos(Fila *f);
void retiraProcesso(Fila *f);

int main(void){

	int escolha;
	Fila *estacionamento = filaCria();
	do{
		menu();
		scanf("%d",&escolha);
		limpaBuffer();
		switch(escolha){
			case 1:
				adicionaProcesso(estacionamento);
				break;
			case 2:
				retiraProcesso(estacionamento);
				break;
			case 3:
				imprimeProcessos(estacionamento);
				break;
			case 0:
				break;
		}
		continua();
	}while(escolha != 0);
	filaLibera(estacionamento);

    return 0;
}

void menu(void){

	printf("Digite:\n");
	printf("1 - Para adicionar processo\n");
	printf("2 - Para retirar um processo\n");
	printf("3 - Para imprimir os processos\n");
	printf("0 - Para sair do programa\n");
}

void adicionaProcesso(Fila *f){

	char *placaTesta;
	int temp,id;
	printf("Digite o identificador do processo(inteiro):");
	scanf("%d",&id);
	

	if(verificaProcesso(id,f)){
		printf("Digite o tempo de espera(inteiro):");
		scanf("%d",&temp);
		filaInsere(f,id,temp);
	}
	else{
		printf("Processo repitido.!!!!!!!!!!!!!!!!!!!!\n");
	}
}

int verificaProcesso(int identificadorTesta,Fila *f){


	Lista *l;
	if(filaVazia(f)){
		return 1;
	}
	else{
		for(l = f->ini;l != NULL;l = l->prox){
			if(identificadorTesta == l->info)
				return 0;
		}
		return 1;
	}
}

void imprimeProcessos(Fila *f){

	if(filaVazia(f)){
		printf("Fila de processo vazia\n");
	}
	else{
		filaImprime(f);
	}
}



void retiraProcesso(Fila *f){

	Lista *q ,*l;
	int maior_tempo = f->ini->tempo;
	int cont,cont1;
	if(filaVazia(f)){

		printf("Sem processos\n");
	}
	else{
		for(q = f->ini,cont=0; q!= NULL; q = q->prox,++cont){
			
			if(maior_tempo == q->tempo){
				
				maior_tempo = q->tempo;
			}
			else if(maior_tempo < q->tempo){
				l = q;
				maior_tempo = q->tempo;	
			}
		}
	}
	
	
	f->ini = listaRetira(f->ini,maior_tempo);

	imprimeProcessos(f);
}
