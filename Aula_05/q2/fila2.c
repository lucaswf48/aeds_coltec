#include <stdio.h>
#include <stdlib.h>
#include "fila2.h"

No2* ins2_ini (No2* ini, float v) {
	 No2* p = (No2*) malloc(sizeof(No2));
	 p->info = v;
	 p->prox = ini;
	 p->ant = NULL;
	 if (ini != NULL) /* verifica se lista não estava vazia */
	 ini->ant = p;
	 return p;
}

No2* ins2_fim (No2* fim, float v) {
	 No2* p = (No2*) malloc(sizeof(No2));
	 p->info = v;
	 p->prox = NULL;
	 p->ant = fim;
	 if (fim != NULL) /* verifica se lista não estava vazia */
	 fim->prox = p;
	 return p;
}

/* função auxiliar: retira do início */
No2* ret2_ini (No2* ini) {
	 No2* p = ini->prox;
	 if (p != NULL) /* verifica se lista não ficou vazia */
	 p->ant = NULL;
	 free(ini);
	 return p;
}
/* função auxiliar: retira do fim */
No2* ret2_fim (No2* fim) {
	 No2* p = fim->ant;
	 if (p != NULL) /* verifica se lista não ficou vazia */
	 p->prox = NULL;
	 free(fim);
	 return p;
}

void insere_ini (Fila2* f, float v) {
	 f->ini = ins2_ini(f->ini,v);
	 if (f->fim==NULL) /* fila antes vazia? */
	 f->fim = f->ini;
}
void insere_fim (Fila2* f, float v) {
	 f->fim = ins2_fim(f->fim,v);
	 if (f->ini==NULL) /* fila antes vazia? */
	 f->ini = f->fim;
}
float retira_ini (Fila2* f) {
	 float v;
	 if (vazia(f)) {
	 printf("Fila vazia.\n");
	 exit(1); /* aborta programa */
	 }
	 v = f->ini->info;
	 f->ini = ret2_ini(f->ini);
	 if (f->ini == NULL) /* fila ficou vazia? */
	 f->fim = NULL;
	 return v;
}
float retira_fim (Fila2* f) {
	 float v;
	 if (vazia(f)) {
	 printf("Fila vazia.\n");
	 exit(1); /* aborta programa */
	 }
	 v = f->fim->info;
	 f->fim = ret2_fim(f->fim);
	 if (f->fim == NULL) /* fila ficou vazia? */
	 f->ini = NULL;
	 return v;
}