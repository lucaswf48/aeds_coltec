#include <stdio.h>
#include <stdlib.h>
#include "fila.h"

/*Fila *fila_cria(){

	Fila *f = (Fila *)malloc(sizeof(Fila));
	f->n=0;
	f->ini=0;
	return f;
}

void fila_insere(Fila *f,float v){
	int fim;
	if(f->n == N){
			printf("Estourou\n");
			exit(1);
	}
	fim = (f->ini + f->n) % N;
	f->vet[fim] = v;
	f->n++;
}

float fila_retira(Fila *f){
	float v;
	if(fila_vazia(f)){
		printf("Fila vazia\n");
		exit(1);
	}

	v = f->vet[f->ini];
	f->ini = (f->ini + 1) % N;
	F->n--;

	return v;	
}

int fila_vazia(Fila *f){

	return (f->n == 0);
}

void fila_libera(Fila *f){
	free(f);
}

void fila_imprime(Fila *f){
	int i;
	for(i = 0;i < f->n;i++){
		printf("%f \n", f->vet[(f->ini+i)%N]);
	}
}*/

Fila *fila_cria(){
	Fila *f = (Fila *)malloc(sizeof(Fila));
	f->ini = f->fim = NULL;
	return f;
}

void fila_insere(Fila *f,char *v){

	Lista *n = (Lista *)malloc(sizeof(Lista));
	strcpy(n->info,v);
	n->prox = NULL;
	if(f->fim != NULL)
		f->fim->prox = n;
	else
		f->ini = n;
	f->fim = n;

}

char* fila_retira(Fila *f){

	Lista *t;
	char v[21];
	if(fila_vazia(f)){
		printf("Vazia\n");
		exit(1);
	}
	t = f->ini;
	strcpy(v ,t->info);
	f->ini = t->prox;
	if(f->ini ==NULL)
		f->fim = NULL;
	free(t);
	return v;
}

int fila_vazia(Fila *f){

	return (f->ini == NULL);
}

void fila_libera(Fila *f){

	Lista *q = f->ini;
	while(q != NULL){

		Lista *t = q->prox;
		free(q);
		q = t;
	}
	free(f);
}
void fila_imprime(Fila *f){
	Lista *q;
	for(q = f->ini; q!= NULL; q = q->prox)
		printf("%s \n", q->info);
}	
int incr(int i){

	return (i+1)% N;
}