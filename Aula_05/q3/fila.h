#define N 100

/*struct fila{

	int n;
	int ini;
	float vet[N];
}*/

struct lista{
	//float info;
	char info[21];
	struct lista *prox;
};

typedef struct lista Lista;
struct fila{

	Lista *ini;
	Lista *fim;
};


typedef struct fila Fila;

Fila *fila_cria();
void fila_insere(Fila *f,char *v);
char* fila_retira(Fila *f);
int fila_vazia(Fila *f);
void fila_libera(Fila *f);
int incr(int i);
