#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "lwfunc.h"

void menu();
void filaInsereString(Fila *f,char *v,int a);
int estacionaCarro(Fila *f);
int verificaPlaca(char *pla,Fila *f);
void imprimeEstacionamento(Fila *f);
void filaImprime(Fila *f);
void retiraEstacionamento(Fila *f);
char* filaRetira(Fila *f,int *a);
Lista *listaInsereOrdenadoIdoso(Lista *l, int v,char *a);

int main(void){

    int escolha;
    Fila *estacionamento = filaCria();
    do{
        limpaTela();
        menu();
        scanf("%d",&escolha);
        switch(escolha){
        	case 0:
        		//continua();
        		break;
            case 1:
                estacionaCarro(estacionamento);
                break;
            case 2:
                retiraEstacionamento(estacionamento);
                break;
            case 3:
                imprimeEstacionamento(estacionamento);
                break;
        }
        continua();
    }while(escolha != 0);
    return 0;
}

void menu(){

    printf("1 - Para estacionar um carro;\n");
    printf("2 - Para retirar um carro;\n");
    printf("3 - Exibir todos os carros;\n");
    printf("Digite:");
}

int estacionaCarro(Fila *f){

    char pla[11];
    int ida;

    printf("Digite a placa do carro:");
    scanf("%s",pla);
    if(!(filaVazia(f))){
         if(verificaPlaca(pla,f)){
            printf("Placa repitida\n");
            return 0;
        }
        else{
        	printf("Digite a sua idade:");
    		scanf("%d",&ida);
            if(ida > 60){

              f->ini = listaInsereOrdenadoIdoso(f->ini,ida,pla);
                return 0;
            }

    		filaInsereString(f,pla,ida);
    		return 1;
        }
    }
    else{
    	printf("Digite a sua idade:");
    	scanf("%d",&ida);
        
        if(ida > 60){

                f->ini = listaInsereOrdenadoIdoso(f->ini,ida,pla);
                filaImprime(f);
                return 0;
        }
    	filaInsereString(f,pla,ida);
    }


    return 1;
}

int verificaPlaca(char *pla,Fila *f){

    Lista *q;
    for(q = f->ini;q!=NULL;q=q->prox){

        if(strcmp(pla,q->placa)==0){
            return 1;
        }
    }

    return 0;
}

void filaInsereString(Fila *f,char *v,int a){

    Lista *n = (Lista *)malloc(sizeof(Lista));
    strcpy(n->placa,v);
    n->idade = a;
    n->prox = NULL;
    if(f->fim != NULL)
        f->fim->prox = n;
    else
        f->ini = n;
    f->fim = n;
}

void imprimeEstacionamento(Fila *f){

    if(!(filaVazia(f)))
        filaImprime(f);
    else{
        printf("Estacionamento vazio");
    }
}

void filaImprime(Fila *f){

    Lista *q;
    int conta_carros;
    for(q = f->ini, conta_carros = 1; q!= NULL; q = q->prox,++conta_carros){
        printf("%d - %s Idade: %d\n", conta_carros,q->placa,q->idade);
    }
}

char* filaRetira(Fila *f,int *a){

    Lista *t;
    char *v = (char*)malloc(11*sizeof(char));
    if(filaVazia(f)){
        printf("Vazia\n");
        exit(1);
    }
    t = f->ini;
    *a = f->ini->idade;
    strcpy(v,f->ini->placa);
    f->ini = t->prox;
    if(f->ini ==NULL)
        f->fim = NULL;
    free(t);
    
    return v;
}

void retiraEstacionamento(Fila *f){

    char kank[11];
    int idades;
    strcpy(kank,f->ini->placa);

    if(!(filaVazia(f))){
        
        char plac[11],*teste,daw2[11];
        printf("Digite a placa do carro:");
        scanf("%s",plac);
        if(strcmp(plac,f->ini->placa) == 0){
            filaRetira(f,&idades);
            return;
        }
        else{

            do{ 

                teste = filaRetira(f,&idades);
                strcpy(daw2,teste);
                filaInsereString(f,daw2,idades);
                if(strcmp (kank,f->ini->placa)==0){
                    printf("Carro não encontrado\n");
                    return;
                }
            }while(strcmp(plac,f->ini->placa)!=0);

            teste = filaRetira(f,&idades);
        }
    }
    else{
        printf("Estacionamento vazio\n");
    }

}

Lista *listaInsereOrdenadoIdoso(Lista *l, int v,char *a){

    Lista *novo;
    Lista *ant = NULL;
    Lista *p = l;

    while(p != NULL && p->idade < v){
        ant = p;
        p = p->prox;
    }

    novo = (Lista*)malloc(sizeof(Lista));
    novo->idade = v;
    strcpy(novo->placa,a);

    if(ant == NULL){
        novo->prox = l;
        l = novo;
    }
    else{
        novo->prox = ant->prox;
        ant->prox = novo;
    }
    return l;
}

