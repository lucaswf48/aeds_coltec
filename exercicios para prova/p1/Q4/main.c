
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "lwfunc.h"
#define DIM1 101

int avaliaExpressao(char *exp);

int main(){

	char *string,expressao[DIM1];

	printf("Digite a expressão matemática:");
	//string = lerStringN();
	scanf("%s",expressao);
	if(avaliaExpressao(expressao))
		printf("Expressão valida\n");
	else
		printf("Expressão invalida\n");




	free(string);
	return 0;
}


int avaliaExpressao(char *exp){

	int cont;
	Pilha *paren = pilhaCria(),*col = pilhaCria();
	for(cont = 0;cont < strlen(exp);++cont){

		if(exp[cont] == ')'){

			if(pilhaVazia(paren)){
				return 0;
			}
			else{
				pilhaPop(paren);
			}
		}
		else if(exp[cont] == '('){
			pilhaPush(paren,exp[cont]);
		}
		else if(exp[cont] == ']'){
            if(pilhaVazia(col)){
				return 0;
			}
			else{
				pilhaPop(col);
			}
		}
		else if(exp[cont] == '['){
            pilhaPush(col,exp[cont]);
		}

	}

	if(pilhaVazia(paren) && pilhaVazia(col))
		return 1;
	else
		return 0;
}